@Entity
@Table(name = NCTSDecl.TABLE_NAME)
public class NCTSDecl {

	public static final String TABLE_NAME = "ncts_declaration";
public static class ColumnNames {
public static final String NCTS_DECLARATION_ID = "ncts_declaration_id";
public static final String REFERENCE_NUMBER = "reference_number";
public static final String DOCUMENT_NUMBER = "document_number";
public static final String TYPE_OF_DECLARATION_CODE = "type_of_declaration_code";
public static final String TYPE_OF_DECLARATION_ID = "type_of_declaration_id";
public static final String TYPE_OF_DECLARATION = "type_of_declaration";
public static final String COUNTRY_OF_DESTINATION_CODE = "country_of_destination_code";
public static final String COUNTRY_OF_DESTINATION_ID = "country_of_destination_id";
public static final String COUNTRY_OF_DESTINATION = "country_of_destination";
public static final String AGREED_LOC_OF_GOODS_CODE = "agreed_loc_of_goods_code";
public static final String AGREED_LOC_OF_GOODS = "agreed_loc_of_goods";
public static final String AGREED_LOC_OF_GOODS_LNG = "agreed_loc_of_goods_lng";
public static final String AUTHORISED_LOC_OF_GOODS_CODE = "authorised_loc_of_goods_code";
public static final String PLACE_OF_LOADING = "place_of_loading";
public static final String COUNTRY_OF_DISPATCH_CODE_CODE = "country_of_dispatch_code_code";
public static final String COUNTRY_OF_DISPATCH_CODE_ID = "country_of_dispatch_code_id";
public static final String COUNTRY_OF_DISPATCH_CODE = "country_of_dispatch_code";
public static final String CUSTOMS_SUB_PLACE = "customs_sub_place";
public static final String INLAND_TRANSPORT_MODE_CODE = "inland_transport_mode_code";
public static final String INLAND_TRANSPORT_MODE_ID = "inland_transport_mode_id";
public static final String INLAND_TRANSPORT_MODE = "inland_transport_mode";
public static final String TRANSPORT_MODE_AT_BORDER_CODE = "transport_mode_at_border_code";
public static final String TRANSPORT_MODE_AT_BORDER_ID = "transport_mode_at_border_id";
public static final String TRANSPORT_MODE_AT_BORDER = "transport_mode_at_border";
public static final String IDENT_OF_MEANS_OF_TRANSP_AT_DEP = "ident_of_means_of_transp_at_dep";
public static final String IDENT_OF_MEANS_OF_TRANP_AT_DEP_LNG = "ident_of_means_of_tranp_at_dep_lng";
public static final String NATION_OF_MEANS_OF_TRANS_AT_DEP = "nation_of_means_of_trans_at_dep";
public static final String IDENT_OF_MEANS_OF_TRANS_CROSS_BRD = "ident_of_means_of_trans_cross_brd";
public static final String IDENT_OF_MEANS_OF_TRANS_CR_BRD_LNG = "ident_of_means_of_trans_cr_brd_lng";
public static final String NATION_OF_MEANS_OF_TRANS_CROSS_BRD = "nation_of_means_of_trans_cross_brd";
public static final String TYPE_OF_MEANS_OF_TRANS_CROSS_BRD = "type_of_means_of_trans_cross_brd";
public static final String CONTAINER_INDICATOR = "container_indicator";
public static final String DIALOG_LNG_INDICATOR_AT_DEP = "dialog_lng_indicator_at_dep";
public static final String NCTS_ACCOMP_DOC_LNG = "ncts_accomp_doc_lng";
public static final String TOTAL_NUM_OF_ITEMS = "total_num_of_items";
public static final String TOTAL_NUM__OF_PACKAGES = "total_num__of_packages";
public static final String TOTAL_GROSS_MASS = "total_gross_mass";
public static final String DECLARATION_DATE = "declaration_date";
public static final String DECLARATION_PLACE = "declaration_place";
public static final String DECLARATION_PLACE_LNG = "declaration_place_lng";
public static final String SPECIAL_CIRCUMSTANCE_INDICATOR_CODE = "special_circumstance_indicator_code";
public static final String SPECIAL_CIRCUMSTANCE_INDICATOR_ID = "special_circumstance_indicator_id";
public static final String SPECIAL_CIRCUMSTANCE_INDICATOR = "special_circumstance_indicator";
public static final String METHOD_OF_PAYMENT_CODE = "method_of_payment_code";
public static final String METHOD_OF_PAYMENT_ID = "method_of_payment_id";
public static final String METHOD_OF_PAYMENT = "method_of_payment";
public static final String COMMERCIAL_REF_NUM = "commercial_ref_num";
public static final String SEC_DATA = "sec_data";
public static final String CON_REFERENCE_NUM = "con_reference_num";
public static final String PLACE_OF_UNLOADING = "place_of_unloading";
public static final String PLACE_OF_UNLOADING_LNG = "place_of_unloading_lng";
public static final String PRINCIPAL_TRADER_SITE_ID_CODE = "principal_trader_site_id_code";
public static final String PRINCIPAL_TRADER_SITE_ID_ID = "principal_trader_site_id_id";
public static final String PRINCIPAL_TRADER_SITE_ID = "principal_trader_site_id";
public static final String PRINCIPAL_TRADER_NAME = "principal_trader_name";
public static final String PRINCIPAL_TRADER_STREET_AND_NUM = "principal_trader_street_and_num";
public static final String PRINCIPAL_TRADER_POSTAL_CODE = "principal_trader_postal_code";
public static final String PRINCIPAL_TRADER_CITY = "principal_trader_city";
public static final String PRINCIPAL_TRADER_COUNTRY_CODE_CODE = "principal_trader_country_code_code";
public static final String PRINCIPAL_TRADER_COUNTRY_CODE_ID = "principal_trader_country_code_id";
public static final String PRINCIPAL_TRADER_COUNTRY_CODE = "principal_trader_country_code";
public static final String PRINCIPAL_TRADER_NAD_LNG = "principal_trader_nad_lng";
public static final String PRINCIPAL_TRADER_TIN = "principal_trader_tin";
public static final String PRINCIPAL_TRADER_HIT = "principal_trader_hit";
public static final String CONSIGNOR_TRADER_SITE_ID_CODE = "consignor_trader_site_id_code";
public static final String CONSIGNOR_TRADER_SITE_ID_ID = "consignor_trader_site_id_id";
public static final String CONSIGNOR_TRADER_SITE_ID = "consignor_trader_site_id";
public static final String CONSIGNOR_TRADER_NAME = "consignor_trader_name";
public static final String CONSIGNOR_TRADER_STREET_AND_NUM = "consignor_trader_street_and_num";
public static final String CONSIGNOR_TRADER_POSTAL_CODE = "consignor_trader_postal_code";
public static final String CONSIGNOR_TRADER_CITY = "consignor_trader_city";
public static final String CONSIGNOR_TRADER_COUNTRY_CODE_CODE = "consignor_trader_country_code_code";
public static final String CONSIGNOR_TRADER_COUNTRY_CODE_ID = "consignor_trader_country_code_id";
public static final String CONSIGNOR_TRADER_COUNTRY_CODE = "consignor_trader_country_code";
public static final String CONSIGNOR_TRADER_NAD_LNG = "consignor_trader_nad_lng";
public static final String CONSIGNOR_TRADER_TIN = "consignor_trader_tin";
public static final String CONSIGNEE_TRADER_SITE_ID_CODE = "consignee_trader_site_id_code";
public static final String CONSIGNEE_TRADER_SITE_ID_ID = "consignee_trader_site_id_id";
public static final String CONSIGNEE_TRADER_SITE_ID = "consignee_trader_site_id";
public static final String CONSIGNEE_TRADER_NAME = "consignee_trader_name";
public static final String CONSIGNEE_TRADER_STREET_AND_NUM = "consignee_trader_street_and_num";
public static final String CONSIGNEE_TRADER_POSTAL_CODE = "consignee_trader_postal_code";
public static final String CONSIGNEE_TRADER_CITY = "consignee_trader_city";
public static final String CONSIGNEE_TRADER_COUNTRY_CODE_CODE = "consignee_trader_country_code_code";
public static final String CONSIGNEE_TRADER_COUNTRY_CODE_ID = "consignee_trader_country_code_id";
public static final String CONSIGNEE_TRADER_COUNTRY_CODE = "consignee_trader_country_code";
public static final String CONSIGNEE_TRADER_NAD_LNG = "consignee_trader_nad_lng";
public static final String CONSIGNEE_TRADER_TIN = "consignee_trader_tin";
public static final String AUTHORISED_CONSIGNEE_SITE_ID_CODE = "authorised_consignee_site_id_code";
public static final String AUTHORISED_CONSIGNEE_SITE_ID_ID = "authorised_consignee_site_id_id";
public static final String AUTHORISED_CONSIGNEE_SITE_ID = "authorised_consignee_site_id";
public static final String AUTHORISED_CONSIGNEE_TRAD_TIN = "authorised_consignee_trad_tin";
public static final String DEPART_CUSTOMS_OFFICE_REF_NUM_CODE = "depart_customs_office_ref_num_code";
public static final String DEPART_CUSTOMS_OFFICE_REF_NUM_ID = "depart_customs_office_ref_num_id";
public static final String DEPART_CUSTOMS_OFFICE_REF_NUM = "depart_customs_office_ref_num";
public static final String DEST_CUSTOMS_OFFICE_REF_NUM_CODE = "dest_customs_office_ref_num_code";
public static final String DEST_CUSTOMS_OFFICE_REF_NUM_ID = "dest_customs_office_ref_num_id";
public static final String DEST_CUSTOMS_OFFICE_REF_NUM = "dest_customs_office_ref_num";
public static final String CONTROL_RESULT_CODE_CODE = "control_result_code_code";
public static final String CONTROL_RESULT_CODE_ID = "control_result_code_id";
public static final String CONTROL_RESULT_CODE = "control_result_code";
public static final String CONTROL_DATE_LIMIT = "control_date_limit";
public static final String REPRESENTATIVE_NAME = "representative_name";
public static final String REPRESENTATIVE_CAPACITY = "representative_capacity";
public static final String REPRESENTATIVE_CAPACITY_LNG = "representative_capacity_lng";
public static final String SEALS_NUMBER = "seals_number";
public static final String CARRIER_SITE_ID_CODE = "carrier_site_id_code";
public static final String CARRIER_SITE_ID_ID = "carrier_site_id_id";
public static final String CARRIER_SITE_ID = "carrier_site_id";
public static final String CARRIER_NAME = "carrier_name";
public static final String CARRIER_STREET_AND_NUM = "carrier_street_and_num";
public static final String CARRIER_POSTAL_CODE = "carrier_postal_code";
public static final String CARRIER_CITY = "carrier_city";
public static final String CARRIER_COUNTRY_CODE_CODE = "carrier_country_code_code";
public static final String CARRIER_COUNTRY_CODE_ID = "carrier_country_code_id";
public static final String CARRIER_COUNTRY_CODE = "carrier_country_code";
public static final String CARRIER_NAD_LNG = "carrier_nad_lng";
public static final String CARRIER_TIN = "carrier_tin";
public static final String SEC_CONSIGNOR_SITE_ID_CODE = "sec_consignor_site_id_code";
public static final String SEC_CONSIGNOR_SITE_ID_ID = "sec_consignor_site_id_id";
public static final String SEC_CONSIGNOR_SITE_ID = "sec_consignor_site_id";
public static final String SEC_CONSIGNOR_NAME = "sec_consignor_name";
public static final String SEC_CONSIGNOR_STREET_AND_NUM = "sec_consignor_street_and_num";
public static final String SEC_CONSIGNOR_POSTAL_CODE = "sec_consignor_postal_code";
public static final String SEC_CONSIGNOR_CITY = "sec_consignor_city";
public static final String SEC_CONSIGNOR_COUNTRY_CODE_CODE = "sec_consignor_country_code_code";
public static final String SEC_CONSIGNOR_COUNTRY_CODE_ID = "sec_consignor_country_code_id";
public static final String SEC_CONSIGNOR_COUNTRY_CODE = "sec_consignor_country_code";
public static final String SEC_CONSIGNOR_NAD_LNG = "sec_consignor_nad_lng";
public static final String SEC_CONSIGNOR_TIN = "sec_consignor_tin";
public static final String SEC_CONSIGNEE_SITE_ID_CODE = "sec_consignee_site_id_code";
public static final String SEC_CONSIGNEE_SITE_ID_ID = "sec_consignee_site_id_id";
public static final String SEC_CONSIGNEE_SITE_ID = "sec_consignee_site_id";
public static final String SEC_CONSIGNEE_NAME = "sec_consignee_name";
public static final String SEC_CONSIGNEE_STREET_AND_NUM = "sec_consignee_street_and_num";
public static final String SEC_CONSIGNEE_POSTAL_CODE = "sec_consignee_postal_code";
public static final String SEC_CONSIGNEE_CITY = "sec_consignee_city";
public static final String SEC_CONSIGNEE_COUNTRY_CODE_CODE = "sec_consignee_country_code_code";
public static final String SEC_CONSIGNEE_COUNTRY_CODE_ID = "sec_consignee_country_code_id";
public static final String SEC_CONSIGNEE_COUNTRY_CODE = "sec_consignee_country_code";
public static final String SEC_CONSIGNEE_NAD_LNG = "sec_consignee_nad_lng";
public static final String SEC_CONSIGNEE_TIN = "sec_consignee_tin";
public static final String ACCEPTANCE_DATE = "acceptance_date";
public static final String NUMBER_OF_LOADING_LISTS = "number_of_loading_lists";
public static final String CONTROL_DATE = "control_date";
public static final String CONTROLLED_BY = "controlled_by";
public static final String CONTROLLED_BY_LNG = "controlled_by_lng";
public static final String RETURN_COPY_CO_REF_NUMBER_CODE = "return_copy_co_ref_number_code";
public static final String RETURN_COPY_CO_REF_NUMBER_ID = "return_copy_co_ref_number_id";
public static final String RETURN_COPY_CO_REF_NUMBER = "return_copy_co_ref_number";
public static final String RETURN_COPY_CO_NAME = "return_copy_co_name";
public static final String RETURN_COPY_CO_STREETAND_NUM = "return_copy_co_streetand_num";
public static final String RETURN_COPY_CO_COUNTRY_CODE_CODE = "return_copy_co_country_code_code";
public static final String RETURN_COPY_CO_COUNTRY_CODE_ID = "return_copy_co_country_code_id";
public static final String RETURN_COPY_CO_COUNTRY_CODE = "return_copy_co_country_code";
public static final String RETURN_COPY_CO_POSTAL_CODE = "return_copy_co_postal_code";
public static final String RETURN_COPY_CO_CITY = "return_copy_co_city";
public static final String RETURN_COPY_CO_NAD_LNG = "return_copy_co_nad_lng";
public static final String DATE_OF_CONTROL = "date_of_control";
public static final String WRITE_OFF_DATE = "write_off_date";
public static final String GUARANTOR_NAME = "guarantor_name";
public static final String GUARANTOR_STREET_AND_NUMBER = "guarantor_street_and_number";
public static final String GUARANTOR_POSTAL_CODE = "guarantor_postal_code";
public static final String GUARANTOR_CITY = "guarantor_city";
public static final String GUARANTOR_COUNTRY_CODE_CODE = "guarantor_country_code_code";
public static final String GUARANTOR_COUNTRY_CODE_ID = "guarantor_country_code_id";
public static final String GUARANTOR_COUNTRY_CODE = "guarantor_country_code";
public static final String GUARANTOR_NAD_LNG = "guarantor_nad_lng";
public static final String GUARANTOR_TIN = "guarantor_tin";
public static final String RELEASE_REQUEST_DATE = "release_request_date";
public static final String STATUS_ID = "status_id";
public static final String CREATED_DATE = "created_date";
public static final String CREATOR_USER_CODE = "creator_user_code";
public static final String CREATOR_USER_ID = "creator_user_id";
public static final String CREATOR_USER = "creator_user";
public static final String MODIFIED_DATE = "modified_date";
public static final String MODIFIER_USER_CODE = "modifier_user_code";
public static final String MODIFIER_USER_ID = "modifier_user_id";
public static final String MODIFIER_USER = "modifier_user";
private ColumnNames() {
		// intentionally empty
	}}
public static class AttributeNames {
public static final String NCTS_DECLARATION_ID = "nctsDeclarationId";
public static final String REFERENCE_NUMBER = "referenceNumber";
public static final String DOCUMENT_NUMBER = "documentNumber";
public static final String TYPE_OF_DECLARATION_CODE = "typeOfDeclarationCode";
public static final String TYPE_OF_DECLARATION_ID = "typeOfDeclarationId";
public static final String TYPE_OF_DECLARATION = "typeOfDeclaration";
public static final String COUNTRY_OF_DESTINATION_CODE = "countryOfDestinationCode";
public static final String COUNTRY_OF_DESTINATION_ID = "countryOfDestinationId";
public static final String COUNTRY_OF_DESTINATION = "countryOfDestination";
public static final String AGREED_LOC_OF_GOODS_CODE = "agreedLocOfGoodsCode";
public static final String AGREED_LOC_OF_GOODS = "agreedLocOfGoods";
public static final String AGREED_LOC_OF_GOODS_LNG = "agreedLocOfGoodsLng";
public static final String AUTHORISED_LOC_OF_GOODS_CODE = "authorisedLocOfGoodsCode";
public static final String PLACE_OF_LOADING = "placeOfLoading";
public static final String COUNTRY_OF_DISPATCH_CODE_CODE = "countryOfDispatchCodeCode";
public static final String COUNTRY_OF_DISPATCH_CODE_ID = "countryOfDispatchCodeId";
public static final String COUNTRY_OF_DISPATCH_CODE = "countryOfDispatchCode";
public static final String CUSTOMS_SUB_PLACE = "customsSubPlace";
public static final String INLAND_TRANSPORT_MODE_CODE = "inlandTransportModeCode";
public static final String INLAND_TRANSPORT_MODE_ID = "inlandTransportModeId";
public static final String INLAND_TRANSPORT_MODE = "inlandTransportMode";
public static final String TRANSPORT_MODE_AT_BORDER_CODE = "transportModeAtBorderCode";
public static final String TRANSPORT_MODE_AT_BORDER_ID = "transportModeAtBorderId";
public static final String TRANSPORT_MODE_AT_BORDER = "transportModeAtBorder";
public static final String IDENT_OF_MEANS_OF_TRANSP_AT_DEP = "identOfMeansOfTranspAtDep";
public static final String IDENT_OF_MEANS_OF_TRANP_AT_DEP_LNG = "identOfMeansOfTranpAtDepLng";
public static final String NATION_OF_MEANS_OF_TRANS_AT_DEP = "nationOfMeansOfTransAtDep";
public static final String IDENT_OF_MEANS_OF_TRANS_CROSS_BRD = "identOfMeansOfTransCrossBrd";
public static final String IDENT_OF_MEANS_OF_TRANS_CR_BRD_LNG = "identOfMeansOfTransCrBrdLng";
public static final String NATION_OF_MEANS_OF_TRANS_CROSS_BRD = "nationOfMeansOfTransCrossBrd";
public static final String TYPE_OF_MEANS_OF_TRANS_CROSS_BRD = "typeOfMeansOfTransCrossBrd";
public static final String CONTAINER_INDICATOR = "containerIndicator";
public static final String DIALOG_LNG_INDICATOR_AT_DEP = "dialogLngIndicatorAtDep";
public static final String NCTS_ACCOMP_DOC_LNG = "nctsAccompDocLng";
public static final String TOTAL_NUM_OF_ITEMS = "totalNumOfItems";
public static final String TOTAL_NUM__OF_PACKAGES = "totalNum_ofPackages";
public static final String TOTAL_GROSS_MASS = "totalGrossMass";
public static final String DECLARATION_DATE = "declarationDate";
public static final String DECLARATION_PLACE = "declarationPlace";
public static final String DECLARATION_PLACE_LNG = "declarationPlaceLng";
public static final String SPECIAL_CIRCUMSTANCE_INDICATOR_CODE = "specialCircumstanceIndicatorCode";
public static final String SPECIAL_CIRCUMSTANCE_INDICATOR_ID = "specialCircumstanceIndicatorId";
public static final String SPECIAL_CIRCUMSTANCE_INDICATOR = "specialCircumstanceIndicator";
public static final String METHOD_OF_PAYMENT_CODE = "methodOfPaymentCode";
public static final String METHOD_OF_PAYMENT_ID = "methodOfPaymentId";
public static final String METHOD_OF_PAYMENT = "methodOfPayment";
public static final String COMMERCIAL_REF_NUM = "commercialRefNum";
public static final String SEC_DATA = "secData";
public static final String CON_REFERENCE_NUM = "conReferenceNum";
public static final String PLACE_OF_UNLOADING = "placeOfUnloading";
public static final String PLACE_OF_UNLOADING_LNG = "placeOfUnloadingLng";
public static final String PRINCIPAL_TRADER_SITE_ID_CODE = "principalTraderSiteIdCode";
public static final String PRINCIPAL_TRADER_SITE_ID_ID = "principalTraderSiteIdId";
public static final String PRINCIPAL_TRADER_SITE_ID = "principalTraderSiteId";
public static final String PRINCIPAL_TRADER_NAME = "principalTraderName";
public static final String PRINCIPAL_TRADER_STREET_AND_NUM = "principalTraderStreetAndNum";
public static final String PRINCIPAL_TRADER_POSTAL_CODE = "principalTraderPostalCode";
public static final String PRINCIPAL_TRADER_CITY = "principalTraderCity";
public static final String PRINCIPAL_TRADER_COUNTRY_CODE_CODE = "principalTraderCountryCodeCode";
public static final String PRINCIPAL_TRADER_COUNTRY_CODE_ID = "principalTraderCountryCodeId";
public static final String PRINCIPAL_TRADER_COUNTRY_CODE = "principalTraderCountryCode";
public static final String PRINCIPAL_TRADER_NAD_LNG = "principalTraderNadLng";
public static final String PRINCIPAL_TRADER_TIN = "principalTraderTin";
public static final String PRINCIPAL_TRADER_HIT = "principalTraderHit";
public static final String CONSIGNOR_TRADER_SITE_ID_CODE = "consignorTraderSiteIdCode";
public static final String CONSIGNOR_TRADER_SITE_ID_ID = "consignorTraderSiteIdId";
public static final String CONSIGNOR_TRADER_SITE_ID = "consignorTraderSiteId";
public static final String CONSIGNOR_TRADER_NAME = "consignorTraderName";
public static final String CONSIGNOR_TRADER_STREET_AND_NUM = "consignorTraderStreetAndNum";
public static final String CONSIGNOR_TRADER_POSTAL_CODE = "consignorTraderPostalCode";
public static final String CONSIGNOR_TRADER_CITY = "consignorTraderCity";
public static final String CONSIGNOR_TRADER_COUNTRY_CODE_CODE = "consignorTraderCountryCodeCode";
public static final String CONSIGNOR_TRADER_COUNTRY_CODE_ID = "consignorTraderCountryCodeId";
public static final String CONSIGNOR_TRADER_COUNTRY_CODE = "consignorTraderCountryCode";
public static final String CONSIGNOR_TRADER_NAD_LNG = "consignorTraderNadLng";
public static final String CONSIGNOR_TRADER_TIN = "consignorTraderTin";
public static final String CONSIGNEE_TRADER_SITE_ID_CODE = "consigneeTraderSiteIdCode";
public static final String CONSIGNEE_TRADER_SITE_ID_ID = "consigneeTraderSiteIdId";
public static final String CONSIGNEE_TRADER_SITE_ID = "consigneeTraderSiteId";
public static final String CONSIGNEE_TRADER_NAME = "consigneeTraderName";
public static final String CONSIGNEE_TRADER_STREET_AND_NUM = "consigneeTraderStreetAndNum";
public static final String CONSIGNEE_TRADER_POSTAL_CODE = "consigneeTraderPostalCode";
public static final String CONSIGNEE_TRADER_CITY = "consigneeTraderCity";
public static final String CONSIGNEE_TRADER_COUNTRY_CODE_CODE = "consigneeTraderCountryCodeCode";
public static final String CONSIGNEE_TRADER_COUNTRY_CODE_ID = "consigneeTraderCountryCodeId";
public static final String CONSIGNEE_TRADER_COUNTRY_CODE = "consigneeTraderCountryCode";
public static final String CONSIGNEE_TRADER_NAD_LNG = "consigneeTraderNadLng";
public static final String CONSIGNEE_TRADER_TIN = "consigneeTraderTin";
public static final String AUTHORISED_CONSIGNEE_SITE_ID_CODE = "authorisedConsigneeSiteIdCode";
public static final String AUTHORISED_CONSIGNEE_SITE_ID_ID = "authorisedConsigneeSiteIdId";
public static final String AUTHORISED_CONSIGNEE_SITE_ID = "authorisedConsigneeSiteId";
public static final String AUTHORISED_CONSIGNEE_TRAD_TIN = "authorisedConsigneeTradTin";
public static final String DEPART_CUSTOMS_OFFICE_REF_NUM_CODE = "departCustomsOfficeRefNumCode";
public static final String DEPART_CUSTOMS_OFFICE_REF_NUM_ID = "departCustomsOfficeRefNumId";
public static final String DEPART_CUSTOMS_OFFICE_REF_NUM = "departCustomsOfficeRefNum";
public static final String DEST_CUSTOMS_OFFICE_REF_NUM_CODE = "destCustomsOfficeRefNumCode";
public static final String DEST_CUSTOMS_OFFICE_REF_NUM_ID = "destCustomsOfficeRefNumId";
public static final String DEST_CUSTOMS_OFFICE_REF_NUM = "destCustomsOfficeRefNum";
public static final String CONTROL_RESULT_CODE_CODE = "controlResultCodeCode";
public static final String CONTROL_RESULT_CODE_ID = "controlResultCodeId";
public static final String CONTROL_RESULT_CODE = "controlResultCode";
public static final String CONTROL_DATE_LIMIT = "controlDateLimit";
public static final String REPRESENTATIVE_NAME = "representativeName";
public static final String REPRESENTATIVE_CAPACITY = "representativeCapacity";
public static final String REPRESENTATIVE_CAPACITY_LNG = "representativeCapacityLng";
public static final String SEALS_NUMBER = "sealsNumber";
public static final String CARRIER_SITE_ID_CODE = "carrierSiteIdCode";
public static final String CARRIER_SITE_ID_ID = "carrierSiteIdId";
public static final String CARRIER_SITE_ID = "carrierSiteId";
public static final String CARRIER_NAME = "carrierName";
public static final String CARRIER_STREET_AND_NUM = "carrierStreetAndNum";
public static final String CARRIER_POSTAL_CODE = "carrierPostalCode";
public static final String CARRIER_CITY = "carrierCity";
public static final String CARRIER_COUNTRY_CODE_CODE = "carrierCountryCodeCode";
public static final String CARRIER_COUNTRY_CODE_ID = "carrierCountryCodeId";
public static final String CARRIER_COUNTRY_CODE = "carrierCountryCode";
public static final String CARRIER_NAD_LNG = "carrierNadLng";
public static final String CARRIER_TIN = "carrierTin";
public static final String SEC_CONSIGNOR_SITE_ID_CODE = "secConsignorSiteIdCode";
public static final String SEC_CONSIGNOR_SITE_ID_ID = "secConsignorSiteIdId";
public static final String SEC_CONSIGNOR_SITE_ID = "secConsignorSiteId";
public static final String SEC_CONSIGNOR_NAME = "secConsignorName";
public static final String SEC_CONSIGNOR_STREET_AND_NUM = "secConsignorStreetAndNum";
public static final String SEC_CONSIGNOR_POSTAL_CODE = "secConsignorPostalCode";
public static final String SEC_CONSIGNOR_CITY = "secConsignorCity";
public static final String SEC_CONSIGNOR_COUNTRY_CODE_CODE = "secConsignorCountryCodeCode";
public static final String SEC_CONSIGNOR_COUNTRY_CODE_ID = "secConsignorCountryCodeId";
public static final String SEC_CONSIGNOR_COUNTRY_CODE = "secConsignorCountryCode";
public static final String SEC_CONSIGNOR_NAD_LNG = "secConsignorNadLng";
public static final String SEC_CONSIGNOR_TIN = "secConsignorTin";
public static final String SEC_CONSIGNEE_SITE_ID_CODE = "secConsigneeSiteIdCode";
public static final String SEC_CONSIGNEE_SITE_ID_ID = "secConsigneeSiteIdId";
public static final String SEC_CONSIGNEE_SITE_ID = "secConsigneeSiteId";
public static final String SEC_CONSIGNEE_NAME = "secConsigneeName";
public static final String SEC_CONSIGNEE_STREET_AND_NUM = "secConsigneeStreetAndNum";
public static final String SEC_CONSIGNEE_POSTAL_CODE = "secConsigneePostalCode";
public static final String SEC_CONSIGNEE_CITY = "secConsigneeCity";
public static final String SEC_CONSIGNEE_COUNTRY_CODE_CODE = "secConsigneeCountryCodeCode";
public static final String SEC_CONSIGNEE_COUNTRY_CODE_ID = "secConsigneeCountryCodeId";
public static final String SEC_CONSIGNEE_COUNTRY_CODE = "secConsigneeCountryCode";
public static final String SEC_CONSIGNEE_NAD_LNG = "secConsigneeNadLng";
public static final String SEC_CONSIGNEE_TIN = "secConsigneeTin";
public static final String ACCEPTANCE_DATE = "acceptanceDate";
public static final String NUMBER_OF_LOADING_LISTS = "numberOfLoadingLists";
public static final String CONTROL_DATE = "controlDate";
public static final String CONTROLLED_BY = "controlledBy";
public static final String CONTROLLED_BY_LNG = "controlledByLng";
public static final String RETURN_COPY_CO_REF_NUMBER_CODE = "returnCopyCoRefNumberCode";
public static final String RETURN_COPY_CO_REF_NUMBER_ID = "returnCopyCoRefNumberId";
public static final String RETURN_COPY_CO_REF_NUMBER = "returnCopyCoRefNumber";
public static final String RETURN_COPY_CO_NAME = "returnCopyCoName";
public static final String RETURN_COPY_CO_STREETAND_NUM = "returnCopyCoStreetandNum";
public static final String RETURN_COPY_CO_COUNTRY_CODE_CODE = "returnCopyCoCountryCodeCode";
public static final String RETURN_COPY_CO_COUNTRY_CODE_ID = "returnCopyCoCountryCodeId";
public static final String RETURN_COPY_CO_COUNTRY_CODE = "returnCopyCoCountryCode";
public static final String RETURN_COPY_CO_POSTAL_CODE = "returnCopyCoPostalCode";
public static final String RETURN_COPY_CO_CITY = "returnCopyCoCity";
public static final String RETURN_COPY_CO_NAD_LNG = "returnCopyCoNadLng";
public static final String DATE_OF_CONTROL = "dateOfControl";
public static final String WRITE_OFF_DATE = "writeOffDate";
public static final String GUARANTOR_NAME = "guarantorName";
public static final String GUARANTOR_STREET_AND_NUMBER = "guarantorStreetAndNumber";
public static final String GUARANTOR_POSTAL_CODE = "guarantorPostalCode";
public static final String GUARANTOR_CITY = "guarantorCity";
public static final String GUARANTOR_COUNTRY_CODE_CODE = "guarantorCountryCodeCode";
public static final String GUARANTOR_COUNTRY_CODE_ID = "guarantorCountryCodeId";
public static final String GUARANTOR_COUNTRY_CODE = "guarantorCountryCode";
public static final String GUARANTOR_NAD_LNG = "guarantorNadLng";
public static final String GUARANTOR_TIN = "guarantorTin";
public static final String RELEASE_REQUEST_DATE = "releaseRequestDate";
public static final String STATUS_ID = "statusId";
public static final String CREATED_DATE = "createdDate";
public static final String CREATOR_USER_CODE = "creatorUserCode";
public static final String CREATOR_USER_ID = "creatorUserId";
public static final String CREATOR_USER = "creatorUser";
public static final String MODIFIED_DATE = "modifiedDate";
public static final String MODIFIER_USER_CODE = "modifierUserCode";
public static final String MODIFIER_USER_ID = "modifierUserId";
public static final String MODIFIER_USER = "modifierUser";
	private AttributeNames() {
		// intentionally empty
	}
}
private Long nctsDeclarationId;
private String referenceNumber;
private String documentNumber;
private String typeOfDeclarationCode;
private Long typeOfDeclarationId;
private MasterData typeOfDeclaration;
private String countryOfDestinationCode;
private Long countryOfDestinationId;
private Country countryOfDestination;
private String agreedLocOfGoodsCode;
private String agreedLocOfGoods;
private String agreedLocOfGoodsLng;
private String authorisedLocOfGoodsCode;
private String placeOfLoading;
private String countryOfDispatchCodeCode;
private Long countryOfDispatchCodeId;
private Country countryOfDispatchCode;
private String customsSubPlace;
private String inlandTransportModeCode;
private Long inlandTransportModeId;
private MasterData inlandTransportMode;
private String transportModeAtBorderCode;
private Long transportModeAtBorderId;
private MasterData transportModeAtBorder;
private String identOfMeansOfTranspAtDep;
private String identOfMeansOfTranpAtDepLng;
private String nationOfMeansOfTransAtDep;
private String identOfMeansOfTransCrossBrd;
private String identOfMeansOfTransCrBrdLng;
private String nationOfMeansOfTransCrossBrd;
private String typeOfMeansOfTransCrossBrd;
private String containerIndicator;
private String dialogLngIndicatorAtDep;
private String nctsAccompDocLng;
private String totalNumOfItems;
private String totalNum_ofPackages;
private String totalGrossMass;
private Date declarationDate;
private String declarationPlace;
private String declarationPlaceLng;
private String specialCircumstanceIndicatorCode;
private Long specialCircumstanceIndicatorId;
private MasterData specialCircumstanceIndicator;
private String methodOfPaymentCode;
private Long methodOfPaymentId;
private MasterData methodOfPayment;
private String commercialRefNum;
private String secData;
private String conReferenceNum;
private String placeOfUnloading;
private String placeOfUnloadingLng;
private String principalTraderSiteIdCode;
private Long principalTraderSiteIdId;
private PartnerSite principalTraderSiteId;
private String principalTraderName;
private String principalTraderStreetAndNum;
private String principalTraderPostalCode;
private String principalTraderCity;
private String principalTraderCountryCodeCode;
private Long principalTraderCountryCodeId;
private Country principalTraderCountryCode;
private String principalTraderNadLng;
private String principalTraderTin;
private String principalTraderHit;
private String consignorTraderSiteIdCode;
private Long consignorTraderSiteIdId;
private PartnerSite consignorTraderSiteId;
private String consignorTraderName;
private String consignorTraderStreetAndNum;
private String consignorTraderPostalCode;
private String consignorTraderCity;
private String consignorTraderCountryCodeCode;
private Long consignorTraderCountryCodeId;
private Country consignorTraderCountryCode;
private String consignorTraderNadLng;
private String consignorTraderTin;
private String consigneeTraderSiteIdCode;
private Long consigneeTraderSiteIdId;
private PartnerSite consigneeTraderSiteId;
private String consigneeTraderName;
private String consigneeTraderStreetAndNum;
private String consigneeTraderPostalCode;
private String consigneeTraderCity;
private String consigneeTraderCountryCodeCode;
private Long consigneeTraderCountryCodeId;
private Country consigneeTraderCountryCode;
private String consigneeTraderNadLng;
private String consigneeTraderTin;
private String authorisedConsigneeSiteIdCode;
private Long authorisedConsigneeSiteIdId;
private PartnerSite authorisedConsigneeSiteId;
private String authorisedConsigneeTradTin;
private String departCustomsOfficeRefNumCode;
private Long departCustomsOfficeRefNumId;
private CustomsOffice departCustomsOfficeRefNum;
private String destCustomsOfficeRefNumCode;
private Long destCustomsOfficeRefNumId;
private CustomsOffice destCustomsOfficeRefNum;
private String controlResultCodeCode;
private Long controlResultCodeId;
private MasterData controlResultCode;
private Date controlDateLimit;
private String representativeName;
private String representativeCapacity;
private String representativeCapacityLng;
private String sealsNumber;
private String carrierSiteIdCode;
private Long carrierSiteIdId;
private PartnerSite carrierSiteId;
private String carrierName;
private String carrierStreetAndNum;
private String carrierPostalCode;
private String carrierCity;
private String carrierCountryCodeCode;
private Long carrierCountryCodeId;
private Country carrierCountryCode;
private String carrierNadLng;
private String carrierTin;
private String secConsignorSiteIdCode;
private Long secConsignorSiteIdId;
private PartnerSite secConsignorSiteId;
private String secConsignorName;
private String secConsignorStreetAndNum;
private String secConsignorPostalCode;
private String secConsignorCity;
private String secConsignorCountryCodeCode;
private Long secConsignorCountryCodeId;
private Country secConsignorCountryCode;
private String secConsignorNadLng;
private String secConsignorTin;
private String secConsigneeSiteIdCode;
private Long secConsigneeSiteIdId;
private PartnerSite secConsigneeSiteId;
private String secConsigneeName;
private String secConsigneeStreetAndNum;
private String secConsigneePostalCode;
private String secConsigneeCity;
private String secConsigneeCountryCodeCode;
private Long secConsigneeCountryCodeId;
private Country secConsigneeCountryCode;
private String secConsigneeNadLng;
private String secConsigneeTin;
private Date acceptanceDate;
private Integer numberOfLoadingLists;
private Date controlDate;
private String controlledBy;
private String controlledByLng;
private String returnCopyCoRefNumberCode;
private Long returnCopyCoRefNumberId;
private CustomsOffice returnCopyCoRefNumber;
private String returnCopyCoName;
private String returnCopyCoStreetandNum;
private String returnCopyCoCountryCodeCode;
private Long returnCopyCoCountryCodeId;
private Country returnCopyCoCountryCode;
private String returnCopyCoPostalCode;
private String returnCopyCoCity;
private String returnCopyCoNadLng;
private Date dateOfControl;
private Date writeOffDate;
private String guarantorName;
private String guarantorStreetAndNumber;
private String guarantorPostalCode;
private String guarantorCity;
private String guarantorCountryCodeCode;
private Long guarantorCountryCodeId;
private Country guarantorCountryCode;
private String guarantorNadLng;
private String guarantorTin;
private Date releaseRequestDate;
private Long statusId;
private Date createdDate;
private String creatorUserCode;
private Long creatorUserId;
private User creatorUser;
private Date modifiedDate;
private String modifierUserCode;
private Long modifierUserId;
private User modifierUser;


			@Column(name = ColumnNames.NCTS_DECLARATION_ID, insertable = false, updatable = false)
			public Long getNctsDeclarationId() {
				return nctsDeclarationId;
			}
		
			public void setNctsDeclarationId(Long nctsDeclarationId) {
				this.nctsDeclarationId = nctsDeclarationId;
			}
			
			@Column(name = ColumnNames.REFERENCE_NUMBER, length = 22)
			public String getReferenceNumber() {
				return referenceNumber;
			}
		
			public void setReferenceNumber(String referenceNumber) {
				this.referenceNumber = referenceNumber;
			}
			
			@Column(name = ColumnNames.DOCUMENT_NUMBER, length = 21)
			public String getDocumentNumber() {
				return documentNumber;
			}
		
			public void setDocumentNumber(String documentNumber) {
				this.documentNumber = documentNumber;
			}
			
			@Column(name = ColumnNames.TYPE_OF_DECLARATION_CODE, length = 9)
			public String getTypeOfDeclarationCode() {
				return typeOfDeclarationCode;
			}
		
			public void setTypeOfDeclarationCode(String typeOfDeclarationCode) {
				this.typeOfDeclarationCode = typeOfDeclarationCode;
			}
			
			@Column(name = ColumnNames.TYPE_OF_DECLARATION_ID, insertable = false, updatable = false)
			public Long getTypeOfDeclarationId() {
				return typeOfDeclarationId;
			}
		
			public void setTypeOfDeclarationId(Long typeOfDeclarationId) {
				this.typeOfDeclarationId = typeOfDeclarationId;
			}
			
			@ManyToOne
			@Column(name = ColumnNames.TYPE_OF_DECLARATION)
			public MasterData getTypeOfDeclaration() {
				return typeOfDeclaration;
			}
		
			public void setTypeOfDeclaration(MasterData typeOfDeclaration) {
				this.typeOfDeclaration = typeOfDeclaration;
			}
			
			@Column(name = ColumnNames.COUNTRY_OF_DESTINATION_CODE, length = 2)
			public String getCountryOfDestinationCode() {
				return countryOfDestinationCode;
			}
		
			public void setCountryOfDestinationCode(String countryOfDestinationCode) {
				this.countryOfDestinationCode = countryOfDestinationCode;
			}
			
			@Column(name = ColumnNames.COUNTRY_OF_DESTINATION_ID, insertable = false, updatable = false)
			public Long getCountryOfDestinationId() {
				return countryOfDestinationId;
			}
		
			public void setCountryOfDestinationId(Long countryOfDestinationId) {
				this.countryOfDestinationId = countryOfDestinationId;
			}
			
			@ManyToOne
			@Column(name = ColumnNames.COUNTRY_OF_DESTINATION)
			public Country getCountryOfDestination() {
				return countryOfDestination;
			}
		
			public void setCountryOfDestination(Country countryOfDestination) {
				this.countryOfDestination = countryOfDestination;
			}
			
			@Column(name = ColumnNames.AGREED_LOC_OF_GOODS_CODE, length = 17)
			public String getAgreedLocOfGoodsCode() {
				return agreedLocOfGoodsCode;
			}
		
			public void setAgreedLocOfGoodsCode(String agreedLocOfGoodsCode) {
				this.agreedLocOfGoodsCode = agreedLocOfGoodsCode;
			}
			
			@Column(name = ColumnNames.AGREED_LOC_OF_GOODS, length = 35)
			public String getAgreedLocOfGoods() {
				return agreedLocOfGoods;
			}
		
			public void setAgreedLocOfGoods(String agreedLocOfGoods) {
				this.agreedLocOfGoods = agreedLocOfGoods;
			}
			
			@Column(name = ColumnNames.AGREED_LOC_OF_GOODS_LNG, length = 2)
			public String getAgreedLocOfGoodsLng() {
				return agreedLocOfGoodsLng;
			}
		
			public void setAgreedLocOfGoodsLng(String agreedLocOfGoodsLng) {
				this.agreedLocOfGoodsLng = agreedLocOfGoodsLng;
			}
			
			@Column(name = ColumnNames.AUTHORISED_LOC_OF_GOODS_CODE, length = 17)
			public String getAuthorisedLocOfGoodsCode() {
				return authorisedLocOfGoodsCode;
			}
		
			public void setAuthorisedLocOfGoodsCode(String authorisedLocOfGoodsCode) {
				this.authorisedLocOfGoodsCode = authorisedLocOfGoodsCode;
			}
			
			@Column(name = ColumnNames.PLACE_OF_LOADING, length = 17)
			public String getPlaceOfLoading() {
				return placeOfLoading;
			}
		
			public void setPlaceOfLoading(String placeOfLoading) {
				this.placeOfLoading = placeOfLoading;
			}
			
			@Column(name = ColumnNames.COUNTRY_OF_DISPATCH_CODE_CODE, length = 2)
			public String getCountryOfDispatchCodeCode() {
				return countryOfDispatchCodeCode;
			}
		
			public void setCountryOfDispatchCodeCode(String countryOfDispatchCodeCode) {
				this.countryOfDispatchCodeCode = countryOfDispatchCodeCode;
			}
			
			@Column(name = ColumnNames.COUNTRY_OF_DISPATCH_CODE_ID, insertable = false, updatable = false)
			public Long getCountryOfDispatchCodeId() {
				return countryOfDispatchCodeId;
			}
		
			public void setCountryOfDispatchCodeId(Long countryOfDispatchCodeId) {
				this.countryOfDispatchCodeId = countryOfDispatchCodeId;
			}
			
			@ManyToOne
			@Column(name = ColumnNames.COUNTRY_OF_DISPATCH_CODE)
			public Country getCountryOfDispatchCode() {
				return countryOfDispatchCode;
			}
		
			public void setCountryOfDispatchCode(Country countryOfDispatchCode) {
				this.countryOfDispatchCode = countryOfDispatchCode;
			}
			
			@Column(name = ColumnNames.CUSTOMS_SUB_PLACE, length = 17)
			public String getCustomsSubPlace() {
				return customsSubPlace;
			}
		
			public void setCustomsSubPlace(String customsSubPlace) {
				this.customsSubPlace = customsSubPlace;
			}
			
			@Column(name = ColumnNames.INLAND_TRANSPORT_MODE_CODE, length = 2)
			public String getInlandTransportModeCode() {
				return inlandTransportModeCode;
			}
		
			public void setInlandTransportModeCode(String inlandTransportModeCode) {
				this.inlandTransportModeCode = inlandTransportModeCode;
			}
			
			@Column(name = ColumnNames.INLAND_TRANSPORT_MODE_ID, insertable = false, updatable = false)
			public Long getInlandTransportModeId() {
				return inlandTransportModeId;
			}
		
			public void setInlandTransportModeId(Long inlandTransportModeId) {
				this.inlandTransportModeId = inlandTransportModeId;
			}
			
			@ManyToOne
			@Column(name = ColumnNames.INLAND_TRANSPORT_MODE)
			public MasterData getInlandTransportMode() {
				return inlandTransportMode;
			}
		
			public void setInlandTransportMode(MasterData inlandTransportMode) {
				this.inlandTransportMode = inlandTransportMode;
			}
			
			@Column(name = ColumnNames.TRANSPORT_MODE_AT_BORDER_CODE, length = 2)
			public String getTransportModeAtBorderCode() {
				return transportModeAtBorderCode;
			}
		
			public void setTransportModeAtBorderCode(String transportModeAtBorderCode) {
				this.transportModeAtBorderCode = transportModeAtBorderCode;
			}
			
			@Column(name = ColumnNames.TRANSPORT_MODE_AT_BORDER_ID, insertable = false, updatable = false)
			public Long getTransportModeAtBorderId() {
				return transportModeAtBorderId;
			}
		
			public void setTransportModeAtBorderId(Long transportModeAtBorderId) {
				this.transportModeAtBorderId = transportModeAtBorderId;
			}
			
			@ManyToOne
			@Column(name = ColumnNames.TRANSPORT_MODE_AT_BORDER)
			public MasterData getTransportModeAtBorder() {
				return transportModeAtBorder;
			}
		
			public void setTransportModeAtBorder(MasterData transportModeAtBorder) {
				this.transportModeAtBorder = transportModeAtBorder;
			}
			
			@Column(name = ColumnNames.IDENT_OF_MEANS_OF_TRANSP_AT_DEP, length = 27)
			public String getIdentOfMeansOfTranspAtDep() {
				return identOfMeansOfTranspAtDep;
			}
		
			public void setIdentOfMeansOfTranspAtDep(String identOfMeansOfTranspAtDep) {
				this.identOfMeansOfTranspAtDep = identOfMeansOfTranspAtDep;
			}
			
			@Column(name = ColumnNames.IDENT_OF_MEANS_OF_TRANP_AT_DEP_LNG, length = 2)
			public String getIdentOfMeansOfTranpAtDepLng() {
				return identOfMeansOfTranpAtDepLng;
			}
		
			public void setIdentOfMeansOfTranpAtDepLng(String identOfMeansOfTranpAtDepLng) {
				this.identOfMeansOfTranpAtDepLng = identOfMeansOfTranpAtDepLng;
			}
			
			@Column(name = ColumnNames.NATION_OF_MEANS_OF_TRANS_AT_DEP, length = 2)
			public String getNationOfMeansOfTransAtDep() {
				return nationOfMeansOfTransAtDep;
			}
		
			public void setNationOfMeansOfTransAtDep(String nationOfMeansOfTransAtDep) {
				this.nationOfMeansOfTransAtDep = nationOfMeansOfTransAtDep;
			}
			
			@Column(name = ColumnNames.IDENT_OF_MEANS_OF_TRANS_CROSS_BRD, length = 27)
			public String getIdentOfMeansOfTransCrossBrd() {
				return identOfMeansOfTransCrossBrd;
			}
		
			public void setIdentOfMeansOfTransCrossBrd(String identOfMeansOfTransCrossBrd) {
				this.identOfMeansOfTransCrossBrd = identOfMeansOfTransCrossBrd;
			}
			
			@Column(name = ColumnNames.IDENT_OF_MEANS_OF_TRANS_CR_BRD_LNG, length = 2)
			public String getIdentOfMeansOfTransCrBrdLng() {
				return identOfMeansOfTransCrBrdLng;
			}
		
			public void setIdentOfMeansOfTransCrBrdLng(String identOfMeansOfTransCrBrdLng) {
				this.identOfMeansOfTransCrBrdLng = identOfMeansOfTransCrBrdLng;
			}
			
			@Column(name = ColumnNames.NATION_OF_MEANS_OF_TRANS_CROSS_BRD, length = 2)
			public String getNationOfMeansOfTransCrossBrd() {
				return nationOfMeansOfTransCrossBrd;
			}
		
			public void setNationOfMeansOfTransCrossBrd(String nationOfMeansOfTransCrossBrd) {
				this.nationOfMeansOfTransCrossBrd = nationOfMeansOfTransCrossBrd;
			}
			
			@Column(name = ColumnNames.TYPE_OF_MEANS_OF_TRANS_CROSS_BRD, length = 2)
			public String getTypeOfMeansOfTransCrossBrd() {
				return typeOfMeansOfTransCrossBrd;
			}
		
			public void setTypeOfMeansOfTransCrossBrd(String typeOfMeansOfTransCrossBrd) {
				this.typeOfMeansOfTransCrossBrd = typeOfMeansOfTransCrossBrd;
			}
			
			@Column(name = ColumnNames.CONTAINER_INDICATOR, length = 1)
			public String getContainerIndicator() {
				return containerIndicator;
			}
		
			public void setContainerIndicator(String containerIndicator) {
				this.containerIndicator = containerIndicator;
			}
			
			@Column(name = ColumnNames.DIALOG_LNG_INDICATOR_AT_DEP, length = 2)
			public String getDialogLngIndicatorAtDep() {
				return dialogLngIndicatorAtDep;
			}
		
			public void setDialogLngIndicatorAtDep(String dialogLngIndicatorAtDep) {
				this.dialogLngIndicatorAtDep = dialogLngIndicatorAtDep;
			}
			
			@Column(name = ColumnNames.NCTS_ACCOMP_DOC_LNG, length = 2)
			public String getNctsAccompDocLng() {
				return nctsAccompDocLng;
			}
		
			public void setNctsAccompDocLng(String nctsAccompDocLng) {
				this.nctsAccompDocLng = nctsAccompDocLng;
			}
			
			@Column(name = ColumnNames.TOTAL_NUM_OF_ITEMS, length = 5)
			public String getTotalNumOfItems() {
				return totalNumOfItems;
			}
		
			public void setTotalNumOfItems(String totalNumOfItems) {
				this.totalNumOfItems = totalNumOfItems;
			}
			
			@Column(name = ColumnNames.TOTAL_NUM__OF_PACKAGES, length = 7)
			public String getTotalNum_ofPackages() {
				return totalNum_ofPackages;
			}
		
			public void setTotalNum_ofPackages(String totalNum_ofPackages) {
				this.totalNum_ofPackages = totalNum_ofPackages;
			}
			
			@Column(name = ColumnNames.TOTAL_GROSS_MASS, length = 11)
			public String getTotalGrossMass() {
				return totalGrossMass;
			}
		
			public void setTotalGrossMass(String totalGrossMass) {
				this.totalGrossMass = totalGrossMass;
			}
			
			@Column(name = ColumnNames.DECLARATION_DATE)
			public Date getDeclarationDate() {
				return declarationDate;
			}
		
			public void setDeclarationDate(Date declarationDate) {
				this.declarationDate = declarationDate;
			}
			
			@Column(name = ColumnNames.DECLARATION_PLACE, length = 35)
			public String getDeclarationPlace() {
				return declarationPlace;
			}
		
			public void setDeclarationPlace(String declarationPlace) {
				this.declarationPlace = declarationPlace;
			}
			
			@Column(name = ColumnNames.DECLARATION_PLACE_LNG, length = 2)
			public String getDeclarationPlaceLng() {
				return declarationPlaceLng;
			}
		
			public void setDeclarationPlaceLng(String declarationPlaceLng) {
				this.declarationPlaceLng = declarationPlaceLng;
			}
			
			@Column(name = ColumnNames.SPECIAL_CIRCUMSTANCE_INDICATOR_CODE, length = 1)
			public String getSpecialCircumstanceIndicatorCode() {
				return specialCircumstanceIndicatorCode;
			}
		
			public void setSpecialCircumstanceIndicatorCode(String specialCircumstanceIndicatorCode) {
				this.specialCircumstanceIndicatorCode = specialCircumstanceIndicatorCode;
			}
			
			@Column(name = ColumnNames.SPECIAL_CIRCUMSTANCE_INDICATOR_ID, insertable = false, updatable = false)
			public Long getSpecialCircumstanceIndicatorId() {
				return specialCircumstanceIndicatorId;
			}
		
			public void setSpecialCircumstanceIndicatorId(Long specialCircumstanceIndicatorId) {
				this.specialCircumstanceIndicatorId = specialCircumstanceIndicatorId;
			}
			
			@ManyToOne
			@Column(name = ColumnNames.SPECIAL_CIRCUMSTANCE_INDICATOR)
			public MasterData getSpecialCircumstanceIndicator() {
				return specialCircumstanceIndicator;
			}
		
			public void setSpecialCircumstanceIndicator(MasterData specialCircumstanceIndicator) {
				this.specialCircumstanceIndicator = specialCircumstanceIndicator;
			}
			
			@Column(name = ColumnNames.METHOD_OF_PAYMENT_CODE, length = 1)
			public String getMethodOfPaymentCode() {
				return methodOfPaymentCode;
			}
		
			public void setMethodOfPaymentCode(String methodOfPaymentCode) {
				this.methodOfPaymentCode = methodOfPaymentCode;
			}
			
			@Column(name = ColumnNames.METHOD_OF_PAYMENT_ID, insertable = false, updatable = false)
			public Long getMethodOfPaymentId() {
				return methodOfPaymentId;
			}
		
			public void setMethodOfPaymentId(Long methodOfPaymentId) {
				this.methodOfPaymentId = methodOfPaymentId;
			}
			
			@ManyToOne
			@Column(name = ColumnNames.METHOD_OF_PAYMENT)
			public MasterData getMethodOfPayment() {
				return methodOfPayment;
			}
		
			public void setMethodOfPayment(MasterData methodOfPayment) {
				this.methodOfPayment = methodOfPayment;
			}
			
			@Column(name = ColumnNames.COMMERCIAL_REF_NUM, length = 70)
			public String getCommercialRefNum() {
				return commercialRefNum;
			}
		
			public void setCommercialRefNum(String commercialRefNum) {
				this.commercialRefNum = commercialRefNum;
			}
			
			@Column(name = ColumnNames.SEC_DATA, length = 1)
			public String getSecData() {
				return secData;
			}
		
			public void setSecData(String secData) {
				this.secData = secData;
			}
			
			@Column(name = ColumnNames.CON_REFERENCE_NUM, length = 35)
			public String getConReferenceNum() {
				return conReferenceNum;
			}
		
			public void setConReferenceNum(String conReferenceNum) {
				this.conReferenceNum = conReferenceNum;
			}
			
			@Column(name = ColumnNames.PLACE_OF_UNLOADING, length = 35)
			public String getPlaceOfUnloading() {
				return placeOfUnloading;
			}
		
			public void setPlaceOfUnloading(String placeOfUnloading) {
				this.placeOfUnloading = placeOfUnloading;
			}
			
			@Column(name = ColumnNames.PLACE_OF_UNLOADING_LNG, length = 2)
			public String getPlaceOfUnloadingLng() {
				return placeOfUnloadingLng;
			}
		
			public void setPlaceOfUnloadingLng(String placeOfUnloadingLng) {
				this.placeOfUnloadingLng = placeOfUnloadingLng;
			}
			
			@Column(name = ColumnNames.PRINCIPAL_TRADER_SITE_ID_CODE, length = 15)
			public String getPrincipalTraderSiteIdCode() {
				return principalTraderSiteIdCode;
			}
		
			public void setPrincipalTraderSiteIdCode(String principalTraderSiteIdCode) {
				this.principalTraderSiteIdCode = principalTraderSiteIdCode;
			}
			
			@Column(name = ColumnNames.PRINCIPAL_TRADER_SITE_ID_ID, insertable = false, updatable = false)
			public Long getPrincipalTraderSiteIdId() {
				return principalTraderSiteIdId;
			}
		
			public void setPrincipalTraderSiteIdId(Long principalTraderSiteIdId) {
				this.principalTraderSiteIdId = principalTraderSiteIdId;
			}
			
			@ManyToOne
			@Column(name = ColumnNames.PRINCIPAL_TRADER_SITE_ID)
			public PartnerSite getPrincipalTraderSiteId() {
				return principalTraderSiteId;
			}
		
			public void setPrincipalTraderSiteId(PartnerSite principalTraderSiteId) {
				this.principalTraderSiteId = principalTraderSiteId;
			}
			
			@Column(name = ColumnNames.PRINCIPAL_TRADER_NAME, length = 35)
			public String getPrincipalTraderName() {
				return principalTraderName;
			}
		
			public void setPrincipalTraderName(String principalTraderName) {
				this.principalTraderName = principalTraderName;
			}
			
			@Column(name = ColumnNames.PRINCIPAL_TRADER_STREET_AND_NUM, length = 35)
			public String getPrincipalTraderStreetAndNum() {
				return principalTraderStreetAndNum;
			}
		
			public void setPrincipalTraderStreetAndNum(String principalTraderStreetAndNum) {
				this.principalTraderStreetAndNum = principalTraderStreetAndNum;
			}
			
			@Column(name = ColumnNames.PRINCIPAL_TRADER_POSTAL_CODE, length = 9)
			public String getPrincipalTraderPostalCode() {
				return principalTraderPostalCode;
			}
		
			public void setPrincipalTraderPostalCode(String principalTraderPostalCode) {
				this.principalTraderPostalCode = principalTraderPostalCode;
			}
			
			@Column(name = ColumnNames.PRINCIPAL_TRADER_CITY, length = 35)
			public String getPrincipalTraderCity() {
				return principalTraderCity;
			}
		
			public void setPrincipalTraderCity(String principalTraderCity) {
				this.principalTraderCity = principalTraderCity;
			}
			
			@Column(name = ColumnNames.PRINCIPAL_TRADER_COUNTRY_CODE_CODE, length = 2)
			public String getPrincipalTraderCountryCodeCode() {
				return principalTraderCountryCodeCode;
			}
		
			public void setPrincipalTraderCountryCodeCode(String principalTraderCountryCodeCode) {
				this.principalTraderCountryCodeCode = principalTraderCountryCodeCode;
			}
			
			@Column(name = ColumnNames.PRINCIPAL_TRADER_COUNTRY_CODE_ID, insertable = false, updatable = false)
			public Long getPrincipalTraderCountryCodeId() {
				return principalTraderCountryCodeId;
			}
		
			public void setPrincipalTraderCountryCodeId(Long principalTraderCountryCodeId) {
				this.principalTraderCountryCodeId = principalTraderCountryCodeId;
			}
			
			@ManyToOne
			@Column(name = ColumnNames.PRINCIPAL_TRADER_COUNTRY_CODE)
			public Country getPrincipalTraderCountryCode() {
				return principalTraderCountryCode;
			}
		
			public void setPrincipalTraderCountryCode(Country principalTraderCountryCode) {
				this.principalTraderCountryCode = principalTraderCountryCode;
			}
			
			@Column(name = ColumnNames.PRINCIPAL_TRADER_NAD_LNG, length = 2)
			public String getPrincipalTraderNadLng() {
				return principalTraderNadLng;
			}
		
			public void setPrincipalTraderNadLng(String principalTraderNadLng) {
				this.principalTraderNadLng = principalTraderNadLng;
			}
			
			@Column(name = ColumnNames.PRINCIPAL_TRADER_TIN, length = 17)
			public String getPrincipalTraderTin() {
				return principalTraderTin;
			}
		
			public void setPrincipalTraderTin(String principalTraderTin) {
				this.principalTraderTin = principalTraderTin;
			}
			
			@Column(name = ColumnNames.PRINCIPAL_TRADER_HIT, length = 17)
			public String getPrincipalTraderHit() {
				return principalTraderHit;
			}
		
			public void setPrincipalTraderHit(String principalTraderHit) {
				this.principalTraderHit = principalTraderHit;
			}
			
			@Column(name = ColumnNames.CONSIGNOR_TRADER_SITE_ID_CODE, length = 15)
			public String getConsignorTraderSiteIdCode() {
				return consignorTraderSiteIdCode;
			}
		
			public void setConsignorTraderSiteIdCode(String consignorTraderSiteIdCode) {
				this.consignorTraderSiteIdCode = consignorTraderSiteIdCode;
			}
			
			@Column(name = ColumnNames.CONSIGNOR_TRADER_SITE_ID_ID, insertable = false, updatable = false)
			public Long getConsignorTraderSiteIdId() {
				return consignorTraderSiteIdId;
			}
		
			public void setConsignorTraderSiteIdId(Long consignorTraderSiteIdId) {
				this.consignorTraderSiteIdId = consignorTraderSiteIdId;
			}
			
			@ManyToOne
			@Column(name = ColumnNames.CONSIGNOR_TRADER_SITE_ID)
			public PartnerSite getConsignorTraderSiteId() {
				return consignorTraderSiteId;
			}
		
			public void setConsignorTraderSiteId(PartnerSite consignorTraderSiteId) {
				this.consignorTraderSiteId = consignorTraderSiteId;
			}
			
			@Column(name = ColumnNames.CONSIGNOR_TRADER_NAME, length = 35)
			public String getConsignorTraderName() {
				return consignorTraderName;
			}
		
			public void setConsignorTraderName(String consignorTraderName) {
				this.consignorTraderName = consignorTraderName;
			}
			
			@Column(name = ColumnNames.CONSIGNOR_TRADER_STREET_AND_NUM, length = 35)
			public String getConsignorTraderStreetAndNum() {
				return consignorTraderStreetAndNum;
			}
		
			public void setConsignorTraderStreetAndNum(String consignorTraderStreetAndNum) {
				this.consignorTraderStreetAndNum = consignorTraderStreetAndNum;
			}
			
			@Column(name = ColumnNames.CONSIGNOR_TRADER_POSTAL_CODE, length = 9)
			public String getConsignorTraderPostalCode() {
				return consignorTraderPostalCode;
			}
		
			public void setConsignorTraderPostalCode(String consignorTraderPostalCode) {
				this.consignorTraderPostalCode = consignorTraderPostalCode;
			}
			
			@Column(name = ColumnNames.CONSIGNOR_TRADER_CITY, length = 35)
			public String getConsignorTraderCity() {
				return consignorTraderCity;
			}
		
			public void setConsignorTraderCity(String consignorTraderCity) {
				this.consignorTraderCity = consignorTraderCity;
			}
			
			@Column(name = ColumnNames.CONSIGNOR_TRADER_COUNTRY_CODE_CODE, length = 2)
			public String getConsignorTraderCountryCodeCode() {
				return consignorTraderCountryCodeCode;
			}
		
			public void setConsignorTraderCountryCodeCode(String consignorTraderCountryCodeCode) {
				this.consignorTraderCountryCodeCode = consignorTraderCountryCodeCode;
			}
			
			@Column(name = ColumnNames.CONSIGNOR_TRADER_COUNTRY_CODE_ID, insertable = false, updatable = false)
			public Long getConsignorTraderCountryCodeId() {
				return consignorTraderCountryCodeId;
			}
		
			public void setConsignorTraderCountryCodeId(Long consignorTraderCountryCodeId) {
				this.consignorTraderCountryCodeId = consignorTraderCountryCodeId;
			}
			
			@ManyToOne
			@Column(name = ColumnNames.CONSIGNOR_TRADER_COUNTRY_CODE)
			public Country getConsignorTraderCountryCode() {
				return consignorTraderCountryCode;
			}
		
			public void setConsignorTraderCountryCode(Country consignorTraderCountryCode) {
				this.consignorTraderCountryCode = consignorTraderCountryCode;
			}
			
			@Column(name = ColumnNames.CONSIGNOR_TRADER_NAD_LNG, length = 2)
			public String getConsignorTraderNadLng() {
				return consignorTraderNadLng;
			}
		
			public void setConsignorTraderNadLng(String consignorTraderNadLng) {
				this.consignorTraderNadLng = consignorTraderNadLng;
			}
			
			@Column(name = ColumnNames.CONSIGNOR_TRADER_TIN, length = 17)
			public String getConsignorTraderTin() {
				return consignorTraderTin;
			}
		
			public void setConsignorTraderTin(String consignorTraderTin) {
				this.consignorTraderTin = consignorTraderTin;
			}
			
			@Column(name = ColumnNames.CONSIGNEE_TRADER_SITE_ID_CODE, length = 15)
			public String getConsigneeTraderSiteIdCode() {
				return consigneeTraderSiteIdCode;
			}
		
			public void setConsigneeTraderSiteIdCode(String consigneeTraderSiteIdCode) {
				this.consigneeTraderSiteIdCode = consigneeTraderSiteIdCode;
			}
			
			@Column(name = ColumnNames.CONSIGNEE_TRADER_SITE_ID_ID, insertable = false, updatable = false)
			public Long getConsigneeTraderSiteIdId() {
				return consigneeTraderSiteIdId;
			}
		
			public void setConsigneeTraderSiteIdId(Long consigneeTraderSiteIdId) {
				this.consigneeTraderSiteIdId = consigneeTraderSiteIdId;
			}
			
			@ManyToOne
			@Column(name = ColumnNames.CONSIGNEE_TRADER_SITE_ID)
			public PartnerSite getConsigneeTraderSiteId() {
				return consigneeTraderSiteId;
			}
		
			public void setConsigneeTraderSiteId(PartnerSite consigneeTraderSiteId) {
				this.consigneeTraderSiteId = consigneeTraderSiteId;
			}
			
			@Column(name = ColumnNames.CONSIGNEE_TRADER_NAME, length = 35)
			public String getConsigneeTraderName() {
				return consigneeTraderName;
			}
		
			public void setConsigneeTraderName(String consigneeTraderName) {
				this.consigneeTraderName = consigneeTraderName;
			}
			
			@Column(name = ColumnNames.CONSIGNEE_TRADER_STREET_AND_NUM, length = 35)
			public String getConsigneeTraderStreetAndNum() {
				return consigneeTraderStreetAndNum;
			}
		
			public void setConsigneeTraderStreetAndNum(String consigneeTraderStreetAndNum) {
				this.consigneeTraderStreetAndNum = consigneeTraderStreetAndNum;
			}
			
			@Column(name = ColumnNames.CONSIGNEE_TRADER_POSTAL_CODE, length = 9)
			public String getConsigneeTraderPostalCode() {
				return consigneeTraderPostalCode;
			}
		
			public void setConsigneeTraderPostalCode(String consigneeTraderPostalCode) {
				this.consigneeTraderPostalCode = consigneeTraderPostalCode;
			}
			
			@Column(name = ColumnNames.CONSIGNEE_TRADER_CITY, length = 35)
			public String getConsigneeTraderCity() {
				return consigneeTraderCity;
			}
		
			public void setConsigneeTraderCity(String consigneeTraderCity) {
				this.consigneeTraderCity = consigneeTraderCity;
			}
			
			@Column(name = ColumnNames.CONSIGNEE_TRADER_COUNTRY_CODE_CODE, length = 2)
			public String getConsigneeTraderCountryCodeCode() {
				return consigneeTraderCountryCodeCode;
			}
		
			public void setConsigneeTraderCountryCodeCode(String consigneeTraderCountryCodeCode) {
				this.consigneeTraderCountryCodeCode = consigneeTraderCountryCodeCode;
			}
			
			@Column(name = ColumnNames.CONSIGNEE_TRADER_COUNTRY_CODE_ID, insertable = false, updatable = false)
			public Long getConsigneeTraderCountryCodeId() {
				return consigneeTraderCountryCodeId;
			}
		
			public void setConsigneeTraderCountryCodeId(Long consigneeTraderCountryCodeId) {
				this.consigneeTraderCountryCodeId = consigneeTraderCountryCodeId;
			}
			
			@ManyToOne
			@Column(name = ColumnNames.CONSIGNEE_TRADER_COUNTRY_CODE)
			public Country getConsigneeTraderCountryCode() {
				return consigneeTraderCountryCode;
			}
		
			public void setConsigneeTraderCountryCode(Country consigneeTraderCountryCode) {
				this.consigneeTraderCountryCode = consigneeTraderCountryCode;
			}
			
			@Column(name = ColumnNames.CONSIGNEE_TRADER_NAD_LNG, length = 2)
			public String getConsigneeTraderNadLng() {
				return consigneeTraderNadLng;
			}
		
			public void setConsigneeTraderNadLng(String consigneeTraderNadLng) {
				this.consigneeTraderNadLng = consigneeTraderNadLng;
			}
			
			@Column(name = ColumnNames.CONSIGNEE_TRADER_TIN, length = 17)
			public String getConsigneeTraderTin() {
				return consigneeTraderTin;
			}
		
			public void setConsigneeTraderTin(String consigneeTraderTin) {
				this.consigneeTraderTin = consigneeTraderTin;
			}
			
			@Column(name = ColumnNames.AUTHORISED_CONSIGNEE_SITE_ID_CODE, length = 15)
			public String getAuthorisedConsigneeSiteIdCode() {
				return authorisedConsigneeSiteIdCode;
			}
		
			public void setAuthorisedConsigneeSiteIdCode(String authorisedConsigneeSiteIdCode) {
				this.authorisedConsigneeSiteIdCode = authorisedConsigneeSiteIdCode;
			}
			
			@Column(name = ColumnNames.AUTHORISED_CONSIGNEE_SITE_ID_ID, insertable = false, updatable = false)
			public Long getAuthorisedConsigneeSiteIdId() {
				return authorisedConsigneeSiteIdId;
			}
		
			public void setAuthorisedConsigneeSiteIdId(Long authorisedConsigneeSiteIdId) {
				this.authorisedConsigneeSiteIdId = authorisedConsigneeSiteIdId;
			}
			
			@ManyToOne
			@Column(name = ColumnNames.AUTHORISED_CONSIGNEE_SITE_ID)
			public PartnerSite getAuthorisedConsigneeSiteId() {
				return authorisedConsigneeSiteId;
			}
		
			public void setAuthorisedConsigneeSiteId(PartnerSite authorisedConsigneeSiteId) {
				this.authorisedConsigneeSiteId = authorisedConsigneeSiteId;
			}
			
			@Column(name = ColumnNames.AUTHORISED_CONSIGNEE_TRAD_TIN, length = 17)
			public String getAuthorisedConsigneeTradTin() {
				return authorisedConsigneeTradTin;
			}
		
			public void setAuthorisedConsigneeTradTin(String authorisedConsigneeTradTin) {
				this.authorisedConsigneeTradTin = authorisedConsigneeTradTin;
			}
			
			@Column(name = ColumnNames.DEPART_CUSTOMS_OFFICE_REF_NUM_CODE, length = 8)
			public String getDepartCustomsOfficeRefNumCode() {
				return departCustomsOfficeRefNumCode;
			}
		
			public void setDepartCustomsOfficeRefNumCode(String departCustomsOfficeRefNumCode) {
				this.departCustomsOfficeRefNumCode = departCustomsOfficeRefNumCode;
			}
			
			@Column(name = ColumnNames.DEPART_CUSTOMS_OFFICE_REF_NUM_ID, insertable = false, updatable = false)
			public Long getDepartCustomsOfficeRefNumId() {
				return departCustomsOfficeRefNumId;
			}
		
			public void setDepartCustomsOfficeRefNumId(Long departCustomsOfficeRefNumId) {
				this.departCustomsOfficeRefNumId = departCustomsOfficeRefNumId;
			}
			
			@ManyToOne
			@Column(name = ColumnNames.DEPART_CUSTOMS_OFFICE_REF_NUM)
			public CustomsOffice getDepartCustomsOfficeRefNum() {
				return departCustomsOfficeRefNum;
			}
		
			public void setDepartCustomsOfficeRefNum(CustomsOffice departCustomsOfficeRefNum) {
				this.departCustomsOfficeRefNum = departCustomsOfficeRefNum;
			}
			
			@Column(name = ColumnNames.DEST_CUSTOMS_OFFICE_REF_NUM_CODE, length = 8)
			public String getDestCustomsOfficeRefNumCode() {
				return destCustomsOfficeRefNumCode;
			}
		
			public void setDestCustomsOfficeRefNumCode(String destCustomsOfficeRefNumCode) {
				this.destCustomsOfficeRefNumCode = destCustomsOfficeRefNumCode;
			}
			
			@Column(name = ColumnNames.DEST_CUSTOMS_OFFICE_REF_NUM_ID, insertable = false, updatable = false)
			public Long getDestCustomsOfficeRefNumId() {
				return destCustomsOfficeRefNumId;
			}
		
			public void setDestCustomsOfficeRefNumId(Long destCustomsOfficeRefNumId) {
				this.destCustomsOfficeRefNumId = destCustomsOfficeRefNumId;
			}
			
			@ManyToOne
			@Column(name = ColumnNames.DEST_CUSTOMS_OFFICE_REF_NUM)
			public CustomsOffice getDestCustomsOfficeRefNum() {
				return destCustomsOfficeRefNum;
			}
		
			public void setDestCustomsOfficeRefNum(CustomsOffice destCustomsOfficeRefNum) {
				this.destCustomsOfficeRefNum = destCustomsOfficeRefNum;
			}
			
			@Column(name = ColumnNames.CONTROL_RESULT_CODE_CODE, length = 2)
			public String getControlResultCodeCode() {
				return controlResultCodeCode;
			}
		
			public void setControlResultCodeCode(String controlResultCodeCode) {
				this.controlResultCodeCode = controlResultCodeCode;
			}
			
			@Column(name = ColumnNames.CONTROL_RESULT_CODE_ID, insertable = false, updatable = false)
			public Long getControlResultCodeId() {
				return controlResultCodeId;
			}
		
			public void setControlResultCodeId(Long controlResultCodeId) {
				this.controlResultCodeId = controlResultCodeId;
			}
			
			@ManyToOne
			@Column(name = ColumnNames.CONTROL_RESULT_CODE)
			public MasterData getControlResultCode() {
				return controlResultCode;
			}
		
			public void setControlResultCode(MasterData controlResultCode) {
				this.controlResultCode = controlResultCode;
			}
			
			@Column(name = ColumnNames.CONTROL_DATE_LIMIT)
			public Date getControlDateLimit() {
				return controlDateLimit;
			}
		
			public void setControlDateLimit(Date controlDateLimit) {
				this.controlDateLimit = controlDateLimit;
			}
			
			@Column(name = ColumnNames.REPRESENTATIVE_NAME, length = 35)
			public String getRepresentativeName() {
				return representativeName;
			}
		
			public void setRepresentativeName(String representativeName) {
				this.representativeName = representativeName;
			}
			
			@Column(name = ColumnNames.REPRESENTATIVE_CAPACITY, length = 35)
			public String getRepresentativeCapacity() {
				return representativeCapacity;
			}
		
			public void setRepresentativeCapacity(String representativeCapacity) {
				this.representativeCapacity = representativeCapacity;
			}
			
			@Column(name = ColumnNames.REPRESENTATIVE_CAPACITY_LNG, length = 2)
			public String getRepresentativeCapacityLng() {
				return representativeCapacityLng;
			}
		
			public void setRepresentativeCapacityLng(String representativeCapacityLng) {
				this.representativeCapacityLng = representativeCapacityLng;
			}
			
			@Column(name = ColumnNames.SEALS_NUMBER, length = 4)
			public String getSealsNumber() {
				return sealsNumber;
			}
		
			public void setSealsNumber(String sealsNumber) {
				this.sealsNumber = sealsNumber;
			}
			
			@Column(name = ColumnNames.CARRIER_SITE_ID_CODE, length = 15)
			public String getCarrierSiteIdCode() {
				return carrierSiteIdCode;
			}
		
			public void setCarrierSiteIdCode(String carrierSiteIdCode) {
				this.carrierSiteIdCode = carrierSiteIdCode;
			}
			
			@Column(name = ColumnNames.CARRIER_SITE_ID_ID, insertable = false, updatable = false)
			public Long getCarrierSiteIdId() {
				return carrierSiteIdId;
			}
		
			public void setCarrierSiteIdId(Long carrierSiteIdId) {
				this.carrierSiteIdId = carrierSiteIdId;
			}
			
			@ManyToOne
			@Column(name = ColumnNames.CARRIER_SITE_ID)
			public PartnerSite getCarrierSiteId() {
				return carrierSiteId;
			}
		
			public void setCarrierSiteId(PartnerSite carrierSiteId) {
				this.carrierSiteId = carrierSiteId;
			}
			
			@Column(name = ColumnNames.CARRIER_NAME, length = 35)
			public String getCarrierName() {
				return carrierName;
			}
		
			public void setCarrierName(String carrierName) {
				this.carrierName = carrierName;
			}
			
			@Column(name = ColumnNames.CARRIER_STREET_AND_NUM, length = 35)
			public String getCarrierStreetAndNum() {
				return carrierStreetAndNum;
			}
		
			public void setCarrierStreetAndNum(String carrierStreetAndNum) {
				this.carrierStreetAndNum = carrierStreetAndNum;
			}
			
			@Column(name = ColumnNames.CARRIER_POSTAL_CODE, length = 9)
			public String getCarrierPostalCode() {
				return carrierPostalCode;
			}
		
			public void setCarrierPostalCode(String carrierPostalCode) {
				this.carrierPostalCode = carrierPostalCode;
			}
			
			@Column(name = ColumnNames.CARRIER_CITY, length = 35)
			public String getCarrierCity() {
				return carrierCity;
			}
		
			public void setCarrierCity(String carrierCity) {
				this.carrierCity = carrierCity;
			}
			
			@Column(name = ColumnNames.CARRIER_COUNTRY_CODE_CODE, length = 2)
			public String getCarrierCountryCodeCode() {
				return carrierCountryCodeCode;
			}
		
			public void setCarrierCountryCodeCode(String carrierCountryCodeCode) {
				this.carrierCountryCodeCode = carrierCountryCodeCode;
			}
			
			@Column(name = ColumnNames.CARRIER_COUNTRY_CODE_ID, insertable = false, updatable = false)
			public Long getCarrierCountryCodeId() {
				return carrierCountryCodeId;
			}
		
			public void setCarrierCountryCodeId(Long carrierCountryCodeId) {
				this.carrierCountryCodeId = carrierCountryCodeId;
			}
			
			@ManyToOne
			@Column(name = ColumnNames.CARRIER_COUNTRY_CODE)
			public Country getCarrierCountryCode() {
				return carrierCountryCode;
			}
		
			public void setCarrierCountryCode(Country carrierCountryCode) {
				this.carrierCountryCode = carrierCountryCode;
			}
			
			@Column(name = ColumnNames.CARRIER_NAD_LNG, length = 2)
			public String getCarrierNadLng() {
				return carrierNadLng;
			}
		
			public void setCarrierNadLng(String carrierNadLng) {
				this.carrierNadLng = carrierNadLng;
			}
			
			@Column(name = ColumnNames.CARRIER_TIN, length = 17)
			public String getCarrierTin() {
				return carrierTin;
			}
		
			public void setCarrierTin(String carrierTin) {
				this.carrierTin = carrierTin;
			}
			
			@Column(name = ColumnNames.SEC_CONSIGNOR_SITE_ID_CODE, length = 15)
			public String getSecConsignorSiteIdCode() {
				return secConsignorSiteIdCode;
			}
		
			public void setSecConsignorSiteIdCode(String secConsignorSiteIdCode) {
				this.secConsignorSiteIdCode = secConsignorSiteIdCode;
			}
			
			@Column(name = ColumnNames.SEC_CONSIGNOR_SITE_ID_ID, insertable = false, updatable = false)
			public Long getSecConsignorSiteIdId() {
				return secConsignorSiteIdId;
			}
		
			public void setSecConsignorSiteIdId(Long secConsignorSiteIdId) {
				this.secConsignorSiteIdId = secConsignorSiteIdId;
			}
			
			@ManyToOne
			@Column(name = ColumnNames.SEC_CONSIGNOR_SITE_ID)
			public PartnerSite getSecConsignorSiteId() {
				return secConsignorSiteId;
			}
		
			public void setSecConsignorSiteId(PartnerSite secConsignorSiteId) {
				this.secConsignorSiteId = secConsignorSiteId;
			}
			
			@Column(name = ColumnNames.SEC_CONSIGNOR_NAME, length = 35)
			public String getSecConsignorName() {
				return secConsignorName;
			}
		
			public void setSecConsignorName(String secConsignorName) {
				this.secConsignorName = secConsignorName;
			}
			
			@Column(name = ColumnNames.SEC_CONSIGNOR_STREET_AND_NUM, length = 35)
			public String getSecConsignorStreetAndNum() {
				return secConsignorStreetAndNum;
			}
		
			public void setSecConsignorStreetAndNum(String secConsignorStreetAndNum) {
				this.secConsignorStreetAndNum = secConsignorStreetAndNum;
			}
			
			@Column(name = ColumnNames.SEC_CONSIGNOR_POSTAL_CODE, length = 9)
			public String getSecConsignorPostalCode() {
				return secConsignorPostalCode;
			}
		
			public void setSecConsignorPostalCode(String secConsignorPostalCode) {
				this.secConsignorPostalCode = secConsignorPostalCode;
			}
			
			@Column(name = ColumnNames.SEC_CONSIGNOR_CITY, length = 35)
			public String getSecConsignorCity() {
				return secConsignorCity;
			}
		
			public void setSecConsignorCity(String secConsignorCity) {
				this.secConsignorCity = secConsignorCity;
			}
			
			@Column(name = ColumnNames.SEC_CONSIGNOR_COUNTRY_CODE_CODE, length = 2)
			public String getSecConsignorCountryCodeCode() {
				return secConsignorCountryCodeCode;
			}
		
			public void setSecConsignorCountryCodeCode(String secConsignorCountryCodeCode) {
				this.secConsignorCountryCodeCode = secConsignorCountryCodeCode;
			}
			
			@Column(name = ColumnNames.SEC_CONSIGNOR_COUNTRY_CODE_ID, insertable = false, updatable = false)
			public Long getSecConsignorCountryCodeId() {
				return secConsignorCountryCodeId;
			}
		
			public void setSecConsignorCountryCodeId(Long secConsignorCountryCodeId) {
				this.secConsignorCountryCodeId = secConsignorCountryCodeId;
			}
			
			@ManyToOne
			@Column(name = ColumnNames.SEC_CONSIGNOR_COUNTRY_CODE)
			public Country getSecConsignorCountryCode() {
				return secConsignorCountryCode;
			}
		
			public void setSecConsignorCountryCode(Country secConsignorCountryCode) {
				this.secConsignorCountryCode = secConsignorCountryCode;
			}
			
			@Column(name = ColumnNames.SEC_CONSIGNOR_NAD_LNG, length = 2)
			public String getSecConsignorNadLng() {
				return secConsignorNadLng;
			}
		
			public void setSecConsignorNadLng(String secConsignorNadLng) {
				this.secConsignorNadLng = secConsignorNadLng;
			}
			
			@Column(name = ColumnNames.SEC_CONSIGNOR_TIN, length = 17)
			public String getSecConsignorTin() {
				return secConsignorTin;
			}
		
			public void setSecConsignorTin(String secConsignorTin) {
				this.secConsignorTin = secConsignorTin;
			}
			
			@Column(name = ColumnNames.SEC_CONSIGNEE_SITE_ID_CODE, length = 15)
			public String getSecConsigneeSiteIdCode() {
				return secConsigneeSiteIdCode;
			}
		
			public void setSecConsigneeSiteIdCode(String secConsigneeSiteIdCode) {
				this.secConsigneeSiteIdCode = secConsigneeSiteIdCode;
			}
			
			@Column(name = ColumnNames.SEC_CONSIGNEE_SITE_ID_ID, insertable = false, updatable = false)
			public Long getSecConsigneeSiteIdId() {
				return secConsigneeSiteIdId;
			}
		
			public void setSecConsigneeSiteIdId(Long secConsigneeSiteIdId) {
				this.secConsigneeSiteIdId = secConsigneeSiteIdId;
			}
			
			@ManyToOne
			@Column(name = ColumnNames.SEC_CONSIGNEE_SITE_ID)
			public PartnerSite getSecConsigneeSiteId() {
				return secConsigneeSiteId;
			}
		
			public void setSecConsigneeSiteId(PartnerSite secConsigneeSiteId) {
				this.secConsigneeSiteId = secConsigneeSiteId;
			}
			
			@Column(name = ColumnNames.SEC_CONSIGNEE_NAME, length = 35)
			public String getSecConsigneeName() {
				return secConsigneeName;
			}
		
			public void setSecConsigneeName(String secConsigneeName) {
				this.secConsigneeName = secConsigneeName;
			}
			
			@Column(name = ColumnNames.SEC_CONSIGNEE_STREET_AND_NUM, length = 35)
			public String getSecConsigneeStreetAndNum() {
				return secConsigneeStreetAndNum;
			}
		
			public void setSecConsigneeStreetAndNum(String secConsigneeStreetAndNum) {
				this.secConsigneeStreetAndNum = secConsigneeStreetAndNum;
			}
			
			@Column(name = ColumnNames.SEC_CONSIGNEE_POSTAL_CODE, length = 9)
			public String getSecConsigneePostalCode() {
				return secConsigneePostalCode;
			}
		
			public void setSecConsigneePostalCode(String secConsigneePostalCode) {
				this.secConsigneePostalCode = secConsigneePostalCode;
			}
			
			@Column(name = ColumnNames.SEC_CONSIGNEE_CITY, length = 35)
			public String getSecConsigneeCity() {
				return secConsigneeCity;
			}
		
			public void setSecConsigneeCity(String secConsigneeCity) {
				this.secConsigneeCity = secConsigneeCity;
			}
			
			@Column(name = ColumnNames.SEC_CONSIGNEE_COUNTRY_CODE_CODE, length = 2)
			public String getSecConsigneeCountryCodeCode() {
				return secConsigneeCountryCodeCode;
			}
		
			public void setSecConsigneeCountryCodeCode(String secConsigneeCountryCodeCode) {
				this.secConsigneeCountryCodeCode = secConsigneeCountryCodeCode;
			}
			
			@Column(name = ColumnNames.SEC_CONSIGNEE_COUNTRY_CODE_ID, insertable = false, updatable = false)
			public Long getSecConsigneeCountryCodeId() {
				return secConsigneeCountryCodeId;
			}
		
			public void setSecConsigneeCountryCodeId(Long secConsigneeCountryCodeId) {
				this.secConsigneeCountryCodeId = secConsigneeCountryCodeId;
			}
			
			@ManyToOne
			@Column(name = ColumnNames.SEC_CONSIGNEE_COUNTRY_CODE)
			public Country getSecConsigneeCountryCode() {
				return secConsigneeCountryCode;
			}
		
			public void setSecConsigneeCountryCode(Country secConsigneeCountryCode) {
				this.secConsigneeCountryCode = secConsigneeCountryCode;
			}
			
			@Column(name = ColumnNames.SEC_CONSIGNEE_NAD_LNG, length = 2)
			public String getSecConsigneeNadLng() {
				return secConsigneeNadLng;
			}
		
			public void setSecConsigneeNadLng(String secConsigneeNadLng) {
				this.secConsigneeNadLng = secConsigneeNadLng;
			}
			
			@Column(name = ColumnNames.SEC_CONSIGNEE_TIN, length = 17)
			public String getSecConsigneeTin() {
				return secConsigneeTin;
			}
		
			public void setSecConsigneeTin(String secConsigneeTin) {
				this.secConsigneeTin = secConsigneeTin;
			}
			
			@Column(name = ColumnNames.ACCEPTANCE_DATE)
			public Date getAcceptanceDate() {
				return acceptanceDate;
			}
		
			public void setAcceptanceDate(Date acceptanceDate) {
				this.acceptanceDate = acceptanceDate;
			}
			
			@Column(name = ColumnNames.NUMBER_OF_LOADING_LISTS)
			public Integer getNumberOfLoadingLists() {
				return numberOfLoadingLists;
			}
		
			public void setNumberOfLoadingLists(Integer numberOfLoadingLists) {
				this.numberOfLoadingLists = numberOfLoadingLists;
			}
			
			@Column(name = ColumnNames.CONTROL_DATE)
			public Date getControlDate() {
				return controlDate;
			}
		
			public void setControlDate(Date controlDate) {
				this.controlDate = controlDate;
			}
			
			@Column(name = ColumnNames.CONTROLLED_BY, length = 35)
			public String getControlledBy() {
				return controlledBy;
			}
		
			public void setControlledBy(String controlledBy) {
				this.controlledBy = controlledBy;
			}
			
			@Column(name = ColumnNames.CONTROLLED_BY_LNG, length = 2)
			public String getControlledByLng() {
				return controlledByLng;
			}
		
			public void setControlledByLng(String controlledByLng) {
				this.controlledByLng = controlledByLng;
			}
			
			@Column(name = ColumnNames.RETURN_COPY_CO_REF_NUMBER_CODE, length = 8)
			public String getReturnCopyCoRefNumberCode() {
				return returnCopyCoRefNumberCode;
			}
		
			public void setReturnCopyCoRefNumberCode(String returnCopyCoRefNumberCode) {
				this.returnCopyCoRefNumberCode = returnCopyCoRefNumberCode;
			}
			
			@Column(name = ColumnNames.RETURN_COPY_CO_REF_NUMBER_ID, insertable = false, updatable = false)
			public Long getReturnCopyCoRefNumberId() {
				return returnCopyCoRefNumberId;
			}
		
			public void setReturnCopyCoRefNumberId(Long returnCopyCoRefNumberId) {
				this.returnCopyCoRefNumberId = returnCopyCoRefNumberId;
			}
			
			@ManyToOne
			@Column(name = ColumnNames.RETURN_COPY_CO_REF_NUMBER)
			public CustomsOffice getReturnCopyCoRefNumber() {
				return returnCopyCoRefNumber;
			}
		
			public void setReturnCopyCoRefNumber(CustomsOffice returnCopyCoRefNumber) {
				this.returnCopyCoRefNumber = returnCopyCoRefNumber;
			}
			
			@Column(name = ColumnNames.RETURN_COPY_CO_NAME, length = 35)
			public String getReturnCopyCoName() {
				return returnCopyCoName;
			}
		
			public void setReturnCopyCoName(String returnCopyCoName) {
				this.returnCopyCoName = returnCopyCoName;
			}
			
			@Column(name = ColumnNames.RETURN_COPY_CO_STREETAND_NUM, length = 35)
			public String getReturnCopyCoStreetandNum() {
				return returnCopyCoStreetandNum;
			}
		
			public void setReturnCopyCoStreetandNum(String returnCopyCoStreetandNum) {
				this.returnCopyCoStreetandNum = returnCopyCoStreetandNum;
			}
			
			@Column(name = ColumnNames.RETURN_COPY_CO_COUNTRY_CODE_CODE, length = 2)
			public String getReturnCopyCoCountryCodeCode() {
				return returnCopyCoCountryCodeCode;
			}
		
			public void setReturnCopyCoCountryCodeCode(String returnCopyCoCountryCodeCode) {
				this.returnCopyCoCountryCodeCode = returnCopyCoCountryCodeCode;
			}
			
			@Column(name = ColumnNames.RETURN_COPY_CO_COUNTRY_CODE_ID, insertable = false, updatable = false)
			public Long getReturnCopyCoCountryCodeId() {
				return returnCopyCoCountryCodeId;
			}
		
			public void setReturnCopyCoCountryCodeId(Long returnCopyCoCountryCodeId) {
				this.returnCopyCoCountryCodeId = returnCopyCoCountryCodeId;
			}
			
			@ManyToOne
			@Column(name = ColumnNames.RETURN_COPY_CO_COUNTRY_CODE)
			public Country getReturnCopyCoCountryCode() {
				return returnCopyCoCountryCode;
			}
		
			public void setReturnCopyCoCountryCode(Country returnCopyCoCountryCode) {
				this.returnCopyCoCountryCode = returnCopyCoCountryCode;
			}
			
			@Column(name = ColumnNames.RETURN_COPY_CO_POSTAL_CODE, length = 9)
			public String getReturnCopyCoPostalCode() {
				return returnCopyCoPostalCode;
			}
		
			public void setReturnCopyCoPostalCode(String returnCopyCoPostalCode) {
				this.returnCopyCoPostalCode = returnCopyCoPostalCode;
			}
			
			@Column(name = ColumnNames.RETURN_COPY_CO_CITY, length = 35)
			public String getReturnCopyCoCity() {
				return returnCopyCoCity;
			}
		
			public void setReturnCopyCoCity(String returnCopyCoCity) {
				this.returnCopyCoCity = returnCopyCoCity;
			}
			
			@Column(name = ColumnNames.RETURN_COPY_CO_NAD_LNG, length = 2)
			public String getReturnCopyCoNadLng() {
				return returnCopyCoNadLng;
			}
		
			public void setReturnCopyCoNadLng(String returnCopyCoNadLng) {
				this.returnCopyCoNadLng = returnCopyCoNadLng;
			}
			
			@Column(name = ColumnNames.DATE_OF_CONTROL)
			public Date getDateOfControl() {
				return dateOfControl;
			}
		
			public void setDateOfControl(Date dateOfControl) {
				this.dateOfControl = dateOfControl;
			}
			
			@Column(name = ColumnNames.WRITE_OFF_DATE)
			public Date getWriteOffDate() {
				return writeOffDate;
			}
		
			public void setWriteOffDate(Date writeOffDate) {
				this.writeOffDate = writeOffDate;
			}
			
			@Column(name = ColumnNames.GUARANTOR_NAME, length = 35)
			public String getGuarantorName() {
				return guarantorName;
			}
		
			public void setGuarantorName(String guarantorName) {
				this.guarantorName = guarantorName;
			}
			
			@Column(name = ColumnNames.GUARANTOR_STREET_AND_NUMBER, length = 35)
			public String getGuarantorStreetAndNumber() {
				return guarantorStreetAndNumber;
			}
		
			public void setGuarantorStreetAndNumber(String guarantorStreetAndNumber) {
				this.guarantorStreetAndNumber = guarantorStreetAndNumber;
			}
			
			@Column(name = ColumnNames.GUARANTOR_POSTAL_CODE, length = 9)
			public String getGuarantorPostalCode() {
				return guarantorPostalCode;
			}
		
			public void setGuarantorPostalCode(String guarantorPostalCode) {
				this.guarantorPostalCode = guarantorPostalCode;
			}
			
			@Column(name = ColumnNames.GUARANTOR_CITY, length = 35)
			public String getGuarantorCity() {
				return guarantorCity;
			}
		
			public void setGuarantorCity(String guarantorCity) {
				this.guarantorCity = guarantorCity;
			}
			
			@Column(name = ColumnNames.GUARANTOR_COUNTRY_CODE_CODE, length = 2)
			public String getGuarantorCountryCodeCode() {
				return guarantorCountryCodeCode;
			}
		
			public void setGuarantorCountryCodeCode(String guarantorCountryCodeCode) {
				this.guarantorCountryCodeCode = guarantorCountryCodeCode;
			}
			
			@Column(name = ColumnNames.GUARANTOR_COUNTRY_CODE_ID, insertable = false, updatable = false)
			public Long getGuarantorCountryCodeId() {
				return guarantorCountryCodeId;
			}
		
			public void setGuarantorCountryCodeId(Long guarantorCountryCodeId) {
				this.guarantorCountryCodeId = guarantorCountryCodeId;
			}
			
			@ManyToOne
			@Column(name = ColumnNames.GUARANTOR_COUNTRY_CODE)
			public Country getGuarantorCountryCode() {
				return guarantorCountryCode;
			}
		
			public void setGuarantorCountryCode(Country guarantorCountryCode) {
				this.guarantorCountryCode = guarantorCountryCode;
			}
			
			@Column(name = ColumnNames.GUARANTOR_NAD_LNG, length = 2)
			public String getGuarantorNadLng() {
				return guarantorNadLng;
			}
		
			public void setGuarantorNadLng(String guarantorNadLng) {
				this.guarantorNadLng = guarantorNadLng;
			}
			
			@Column(name = ColumnNames.GUARANTOR_TIN, length = 17)
			public String getGuarantorTin() {
				return guarantorTin;
			}
		
			public void setGuarantorTin(String guarantorTin) {
				this.guarantorTin = guarantorTin;
			}
			
			@Column(name = ColumnNames.RELEASE_REQUEST_DATE)
			public Date getReleaseRequestDate() {
				return releaseRequestDate;
			}
		
			public void setReleaseRequestDate(Date releaseRequestDate) {
				this.releaseRequestDate = releaseRequestDate;
			}
			
			@Column(name = ColumnNames.STATUS_ID, insertable = false, updatable = false)
			public Long getStatusId() {
				return statusId;
			}
		
			public void setStatusId(Long statusId) {
				this.statusId = statusId;
			}
			
			@Column(name = ColumnNames.CREATED_DATE)
			public Date getCreatedDate() {
				return createdDate;
			}
		
			public void setCreatedDate(Date createdDate) {
				this.createdDate = createdDate;
			}
			
			@Column(name = ColumnNames.CREATOR_USER_CODE, length = 15)
			public String getCreatorUserCode() {
				return creatorUserCode;
			}
		
			public void setCreatorUserCode(String creatorUserCode) {
				this.creatorUserCode = creatorUserCode;
			}
			
			@Column(name = ColumnNames.CREATOR_USER_ID, insertable = false, updatable = false)
			public Long getCreatorUserId() {
				return creatorUserId;
			}
		
			public void setCreatorUserId(Long creatorUserId) {
				this.creatorUserId = creatorUserId;
			}
			
			@ManyToOne
			@Column(name = ColumnNames.CREATOR_USER)
			public User getCreatorUser() {
				return creatorUser;
			}
		
			public void setCreatorUser(User creatorUser) {
				this.creatorUser = creatorUser;
			}
			
			@Column(name = ColumnNames.MODIFIED_DATE)
			public Date getModifiedDate() {
				return modifiedDate;
			}
		
			public void setModifiedDate(Date modifiedDate) {
				this.modifiedDate = modifiedDate;
			}
			
			@Column(name = ColumnNames.MODIFIER_USER_CODE, length = 15)
			public String getModifierUserCode() {
				return modifierUserCode;
			}
		
			public void setModifierUserCode(String modifierUserCode) {
				this.modifierUserCode = modifierUserCode;
			}
			
			@Column(name = ColumnNames.MODIFIER_USER_ID, insertable = false, updatable = false)
			public Long getModifierUserId() {
				return modifierUserId;
			}
		
			public void setModifierUserId(Long modifierUserId) {
				this.modifierUserId = modifierUserId;
			}
			
			@ManyToOne
			@Column(name = ColumnNames.MODIFIER_USER)
			public User getModifierUser() {
				return modifierUser;
			}
		
			public void setModifierUser(User modifierUser) {
				this.modifierUser = modifierUser;
			}
			}