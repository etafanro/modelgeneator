﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModelGenerator
{
    public partial class Form1 : Form
    {
        private string modelName;
        private string modelPath;
        private List<Attribute> modelAttributes = new List<Attribute>();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string[] lines = System.IO.File.ReadAllLines("types.txt");
            foreach (string line in lines)
            {
                typeList.Items.Add(line);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            File.WriteAllText(modelPath + toModelName(modelName) + ".model.ts", generateModel());
            textBox1.Clear();
            textBox2.Clear();
            modelAttributes.Clear();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            File.WriteAllText(modelPath + toModelName(modelName) + ".list-model.ts", generateListModel());
        }

        private string generateModel()
        {
            modelName = toUpper(textBox1.Text);
            string lines = "import { CustomFormGroup, ServerAttributeValidator } from \"clare-forms\";" + Environment.NewLine;
            lines += "import { FormBuilder } from \"@angular/forms\";" + Environment.NewLine;
            lines += Environment.NewLine;
            lines += "export interface " + modelName + "ModelSource" + Environment.NewLine;
            lines += "{" + Environment.NewLine;
            foreach (Attribute elem in modelAttributes)
            {
                lines += "\t" + elem.name + ": " + elem.type + " | null;" + Environment.NewLine;
            }
            lines += "}" + Environment.NewLine;
            lines += Environment.NewLine;
            lines += "export class " + modelName + "Model extends CustomFormGroup<" + modelName + "ModelSource> {" + Environment.NewLine;
            foreach (Attribute elem in modelAttributes)
            {
                lines += "\tpublic static readonly " + toCaseContant(elem.name) + ": string = \"" + elem.name + "\";" + Environment.NewLine;
            }
            lines += Environment.NewLine;
            lines += "\tpublic static getEmptyModel(formBuilder: FormBuilder): " + modelName + "Model {" + Environment.NewLine;

            lines += "\t\tlet model: " + modelName + "Model = new " + modelName + "Model(formBuilder.group({" + Environment.NewLine;

            int index = 0;
            foreach (Attribute elem in modelAttributes)
            {
                lines += "\t\t\t[" + modelName + "Model." + toCaseContant(elem.name) + "]: [" + checkIfBoolean(elem.type) + ", new ServerAttributeValidator()]"+ checkIfLastAttribute(index) + Environment.NewLine;
                index++;
            }
            lines += "\t\t}, null), null);" + Environment.NewLine;
            lines += Environment.NewLine;
            lines += "\t\treturn model;" + Environment.NewLine;
            lines += "\t}" + Environment.NewLine;
            lines += Environment.NewLine;
            lines += "\tpublic setSource(fromSource: " + modelName + "ModelSource | null): void {" + Environment.NewLine;
            lines += "\t\tthis.sourceParameter = fromSource;" + Environment.NewLine;
            lines += Environment.NewLine;
            foreach (Attribute elem in modelAttributes)
            {
                lines += "\t\tthis.controls[" + modelName + "Model." + toCaseContant(elem.name) + "].setValue(fromSource ? fromSource." + elem.name + " : null);" + Environment.NewLine;
            }
            lines += "\t}" + Environment.NewLine;
            lines += Environment.NewLine;
            lines += "\tpublic toSourceType(): " + modelName + "ModelSource {" + Environment.NewLine;
            lines += "\t\tlet result: " + modelName + "ModelSource = {" + Environment.NewLine;

            index = 0;
            foreach (Attribute elem in modelAttributes)
            {
                lines += "\t\t\t" + elem.name + ": this.controls[" + modelName + "Model." + toCaseContant(elem.name) + "].value"+ checkIfLastAttribute(index) + Environment.NewLine;
                index++;
            }
            lines += "\t\t};" + Environment.NewLine;
            lines += Environment.NewLine;
            lines += "\t\treturn result;" + Environment.NewLine;
            lines += "\t}" + Environment.NewLine;
            lines += "}" + Environment.NewLine;
            return lines;
        }

        private string generateListModel()
        {
            modelName = toUpper(textBox1.Text);
            string lines = "import { " + modelName + "ModelSource, " + modelName + "Model } from \"./" + modelName + ".model\"" + Environment.NewLine;
            lines += "import { FormBuilder, FormArray } from \"@angular/forms\"" + Environment.NewLine;
            lines += "import { ErrorElement, CustomFormGroup } from \"clare-forms\"" + Environment.NewLine;
            lines += "export interface " + modelName + "ListModelSource" + Environment.NewLine;
            lines += "{" + Environment.NewLine;
            lines += "list: " + modelName + "ModelSource[];" + Environment.NewLine;
            lines += "}" + Environment.NewLine;
            lines += "export class " + modelName + "ListModel extends CustomFormGroup<" + modelName + "ListModelSource> {" + Environment.NewLine;
            lines += "public static readonly LIST: string = \"list\"" + Environment.NewLine;
            lines += "public static readonly DELETED_LIST: string = \"deletedList\"" + Environment.NewLine;
            lines += Environment.NewLine;
            lines += "public static getEmptyModel(formBuilder: FormBuilder): " + modelName + "ListModel {" + Environment.NewLine;
            lines += "let itemListModel: " + modelName + "ListModel = new " + modelName + "ListModel(formBuilder.group({" + Environment.NewLine;
            lines += "[" + modelName + "ListModel.LIST]: formBuilder.array([])" + Environment.NewLine;
            lines += "[" + modelName + "ListModel.DELETED_LIST]: formBuilder.array([])" + Environment.NewLine;
            lines += "}, null), null);" + Environment.NewLine;
            lines += Environment.NewLine;
		    lines += "return itemListModel;" + Environment.NewLine;
            lines += "}" + Environment.NewLine;
            lines += Environment.NewLine;
            lines += "public get" + modelName + "List(): FormArray {" + Environment.NewLine;
            lines += "return (<FormArray>this.controls[" + modelName + "ListModel.LIST]);" + Environment.NewLine;
            lines += "}" + Environment.NewLine;
            lines += Environment.NewLine;
            lines += "public getDeleted" + modelName + "List(): FormArray {" + Environment.NewLine;
            lines += "return (<FormArray>this.controls[" + modelName + "ListModel.DELETED_LIST]);" + Environment.NewLine;
            lines += "}" + Environment.NewLine;
            lines += Environment.NewLine;
            lines += "public setSourceWithList(formBuilder: FormBuilder, list: " + modelName + "ModelSource[]): void {" + Environment.NewLine;
            lines += "this.setSource(formBuilder, { list: list, deletedItems: [] });" + Environment.NewLine;
	        lines += "}" + Environment.NewLine;
            lines += Environment.NewLine;
	        lines += "public setSource(formBuilder: FormBuilder, fromSource: " + modelName + "ListModelSource | null): void {" + Environment.NewLine;
            lines += "this.sourceParameter = fromSource;" + Environment.NewLine;
            lines += "let itemModelFormArray: " + modelName + "Model[] = [];" + Environment.NewLine;
		    lines += "if (fromSource && fromSource.list) {" + Environment.NewLine;
            lines += " fromSource.list.forEach(item => {" + Environment.NewLine;    
            lines += "let nCTSDeclarationItemModel = " + modelName + "Model.getEmptyModel(formBuilder);" + Environment.NewLine;
            lines += "nCTSDeclarationItemModel.setSource(item);" + Environment.NewLine;
            lines += "itemModelFormArray.push(nCTSDeclarationItemModel);" + Environment.NewLine;
            lines += "});" + Environment.NewLine;
            lines += "}" + Environment.NewLine;
            lines += Environment.NewLine;
		    lines += "while ((<FormArray>this.controls [" + modelName + "ListModel.LIST]).length > 0)" + Environment.NewLine;
            lines += "(<FormArray>this.controls [" + modelName + "ListModel.LIST]).removeAt(0);" + Environment.NewLine;
            lines += Environment.NewLine;
            lines += "itemModelFormArray.forEach((item) => {" + Environment.NewLine;
            lines += "(< FormArray > this.controls[" + modelName + "ListModel.LIST]).push(item);" + Environment.NewLine;
            lines += "});" + Environment.NewLine;
            lines += Environment.NewLine;
            lines += "while ((<FormArray>this.controls [" + modelName + "ListModel.DELETED_LIST]).length > 0)" + Environment.NewLine;
            lines += "(<FormArray>this.controls [" + modelName + "ListModel.DELETED_LIST]).removeAt(0);" + Environment.NewLine;
            lines += Environment.NewLine;
            lines += "}" + Environment.NewLine;
            lines += Environment.NewLine;
            lines += "public toSourceType(): " + modelName + "ListModelSource {" + Environment.NewLine;
            lines += "let list: " + modelName + "ModelSource[] = [];" + Environment.NewLine;
            lines += "  this.get" + modelName + "List().controls.forEach((element: " + modelName + "Model) => {" + Environment.NewLine;
            lines += "list.push((< " + modelName + "Model > element).toSourceType());" + Environment.NewLine;
            lines += "});" + Environment.NewLine;
            lines += Environment.NewLine;
            lines += "let deletedItemList: " + modelName + "ModelSource[] = [];" + Environment.NewLine;
            lines += "this.getDeleted" + modelName + "List().controls.forEach((element: " + modelName + "Model) => {" + Environment.NewLine;
            lines += "deletedItemList.push((< " + modelName + "Model > element).toSourceType());" + Environment.NewLine;
            lines += "});" + Environment.NewLine;
            lines += Environment.NewLine;
            lines += "let result: " + modelName + "ListModelSource = {" + Environment.NewLine;
            lines += "list: list," + Environment.NewLine;
            lines += "deletedItems: deletedItemList" + Environment.NewLine;
            lines += "};" + Environment.NewLine;
            lines += Environment.NewLine;
		    lines += "return result;" + Environment.NewLine;
            lines += "}" + Environment.NewLine;
            lines += Environment.NewLine;
            lines += "public setErrors(_: ErrorElement | null): void {" + Environment.NewLine;
            lines += "}" + Environment.NewLine;
            lines += "}" + Environment.NewLine;
            return lines;
        }

        private string checkIfLastAttribute(int index)
        {
            if (index == modelAttributes.Count - 1)
                return "";
            else
                return ",";
        }

        private string checkIfBoolean(string type)
        {
            if (type.Equals("boolean"))
                return "false";
            else
                return "null";
        }

        private string toModelName(string the_string)
        {
            if (the_string == null) return the_string;

            string result = the_string.Substring(0, 1).ToLower();

            for (int i = 1; i < the_string.Length; i++)
            {
                if (char.IsUpper(the_string[i])) result += "-";
                result += the_string[i];
            }

            return result.ToLower();
        }

        private string toCaseContant(string the_string)
        {
            if (the_string == null) return the_string;

            string result = the_string.Substring(0, 1).ToUpper();

            for (int i = 1; i < the_string.Length; i++)
            {
                if (char.IsUpper(the_string[i])) result += "_";
                result += the_string[i];
            }

            return result.ToUpper();
        }

        private string toUpper(string word)
        {

            return word.First().ToString().ToUpper() + word.Substring(1);
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            
            if (textBox2.Text.Length > 0 && typeList.SelectedIndex >= 0)
            {
                Attribute attr = new Attribute();
                attr.name = textBox2.Text;
                attr.type = typeList.SelectedItem.ToString();
                modelAttributes.Add(attr);
                ListViewItem item = new ListViewItem(textBox2.Text);
                item.SubItems.Add(typeList.SelectedItem.ToString());
                listView1.Items.Add(item);
                textBox2.Clear();
                textBox3.Text = generateModel();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (typeList.SelectedIndex >= 0)
            {
                modelAttributes.RemoveAt(listView1.Items.IndexOf(listView1.SelectedItems[0]));
                listView1.Items.RemoveAt(listView1.Items.IndexOf(listView1.SelectedItems[0]));
                textBox3.Text = generateModel();
            }
        }

        private void folderBtn_Click(object sender, EventArgs e)
        {
            folderBrowserDialog.ShowDialog();
            modelPath = folderBrowserDialog.SelectedPath;
            label6.Text = modelPath;
        }
    }

    public class Attribute
    {
        public string name { get; set; }
        public string type { get; set; }
    }

}
