package hu.regens.nctsdeclarationmodels.models;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import hu.regens.nctsdeclarationmodels.models.common.Country;
import hu.regens.nctsdeclarationmodels.models.common.CustomsOffice;
import hu.regens.nctsdeclarationmodels.models.common.LocalCarrierSite;
import hu.regens.nctsdeclarationmodels.models.common.LocalConsigneeTraderSite;
import hu.regens.nctsdeclarationmodels.models.common.LocalConsignorTraderSite;
import hu.regens.nctsdeclarationmodels.models.common.LocalGuarantorSite;
import hu.regens.nctsdeclarationmodels.models.common.LocalPrincipalTraderSite;
import hu.regens.nctsdeclarationmodels.models.common.LocalReturnCopyCo;
import hu.regens.nctsdeclarationmodels.models.common.LocalSecConsigneeSite;
import hu.regens.nctsdeclarationmodels.models.common.LocalSecConsignorSite;
import hu.regens.nctsdeclarationmodels.models.common.MasterData;
import hu.regens.nctsdeclarationmodels.models.common.PartnerSite;
import hu.regens.nctsdeclarationmodels.models.common.User;

@Entity
@Table(name = NCTSDecl.TABLE_NAME)
public class NCTSDecl {

	public static final String TABLE_NAME = "ncts_declaration";

	public static class ColumnNames {

		public static final String NCTS_DECLARATION_ID = "ncts_declaration_id";
		public static final String REFERENCE_NUMBER = "reference_number";
		public static final String DOCUMENT_NUMBER = "document_number";
		public static final String TYPE_OF_DECLARATION_ID = "type_of_declaration_id";
		public static final String TYPE_OF_DECLARATION_CODE = "type_of_declaration_code";
		public static final String COUNTRY_OF_DESTINATION_ID = "country_of_destination_id";
		public static final String COUNTRY_OF_DESTINATION_CODE = "country_of_destination_code";
		public static final String AGREED_LOC_OF_GOODS_CODE = "agreed_loc_of_goods_code";
		public static final String AGREED_LOC_OF_GOODS = "agreed_loc_of_goods";
		public static final String AGREED_LOC_OF_GOODS_LNG = "agreed_loc_of_goods_lng";
		public static final String AUTHORISED_LOC_OF_GOODS_CODE = "authorised_loc_of_goods_code";
		public static final String PLACE_OF_LOADING = "place_of_loading";
		public static final String COUNTRY_OF_DISPATCH_CODE = "country_of_dispatch_code";
		public static final String COUNTRY_OF_DISPATCH_ID = "country_of_dispatch_id";
		public static final String CUSTOMS_SUB_PLACE = "customs_sub_place";
		public static final String INLAND_TRANSPORT_MODE_CODE = "inland_transport_mode_code";
		public static final String INLAND_TRANSPORT_MODE_ID = "inland_transport_mode_id";
		public static final String TRANSPORT_MODE_AT_BORDER_CODE = "transport_mode_at_border_code";
		public static final String TRANSPORT_MODE_AT_BORDER_ID = "transport_mode_at_border_id";
		public static final String IDENT_OF_MEANS_OF_TRANSP_AT_DEP = "ident_of_means_of_transp_at_dep";
		public static final String IDENT_OF_MEANS_OF_TRANP_AT_DEP_LNG = "ident_of_means_of_tranp_at_dep_lng";
		public static final String NATION_OF_MEANS_OF_TRANS_AT_DEP_ID = "nation_of_means_of_trans_at_dep_id";
		public static final String NATION_OF_MEANS_OF_TRANS_AT_DEP_CODE = "nation_of_means_of_trans_at_dep_code";
		public static final String IDENT_OF_MEANS_OF_TRANS_CROSS_BRD = "ident_of_means_of_trans_cross_brd";
		public static final String IDENT_OF_MEANS_OF_TRANS_CR_BRD_LNG = "ident_of_means_of_trans_cr_brd_lng";
		public static final String NATION_OF_MEANS_OF_TRANS_CROSS_BRD_ID = "nation_of_means_of_trans_cross_brd";
		public static final String NATION_OF_MEANS_OF_TRANS_CROSS_BRD_CODE = "nation_of_means_of_trans_cross_brd_code";
		public static final String TYPE_OF_MEANS_OF_TRANS_CROSS_BRD = "type_of_means_of_trans_cross_brd";
		public static final String CONTAINER_INDICATOR = "container_indicator";
		public static final String DIALOG_LNG_INDICATOR_AT_DEP = "dialog_lng_indicator_at_dep";
		public static final String NCTS_ACCOMP_DOC_LNG = "ncts_accomp_doc_lng";
		public static final String TOTAL_NUM_OF_ITEMS = "total_num_of_items";
		public static final String TOTAL_NUM_OF_PACKAGES = "total_num_of_packages";
		public static final String TOTAL_GROSS_MASS = "total_gross_mass";
		public static final String DECLARATION_DATE = "declaration_date";
		public static final String DECLARATION_PLACE = "declaration_place";
		public static final String DECLARATION_PLACE_LNG = "declaration_place_lng";
		public static final String SPECIAL_CIRCUMSTANCE_INDICATOR_ID = "special_circumstance_indicator_id";
		public static final String SPECIAL_CIRCUMSTANCE_INDICATOR_CODE = "special_circumstance_indicator_code";
		public static final String METHOD_OF_PAYMENT_CODE = "method_of_payment_code";
		public static final String METHOD_OF_PAYMENT_ID = "method_of_payment_id";
		public static final String COMMERCIAL_REF_NUM = "commercial_ref_num";
		public static final String SEC_DATA = "sec_data";
		public static final String CON_REFERENCE_NUM = "con_reference_num";
		public static final String PLACE_OF_UNLOADING = "place_of_unloading";
		public static final String PLACE_OF_UNLOADING_LNG = "place_of_unloading_lng";
		public static final String PRINCIPAL_TRADER_SITE_ID = "principal_trader_site_id";
		public static final String PRINCIPAL_TRADER_NAME = "principal_trader_name";
		public static final String PRINCIPAL_TRADER_STREET_AND_NUM = "principal_trader_street_and_num";
		public static final String PRINCIPAL_TRADER_POSTAL_CODE = "principal_trader_postal_code";
		public static final String PRINCIPAL_TRADER_CITY = "principal_trader_city";
		public static final String PRINCIPAL_TRADER_COUNTRY_CODE = "principal_trader_country_code";
		public static final String PRINCIPAL_TRADER_COUNTRY_ID = "principal_trader_country_id";
		public static final String PRINCIPAL_TRADER_NAD_LNG = "principal_trader_nad_lng";
		public static final String PRINCIPAL_TRADER_TIN = "principal_trader_tin";
		public static final String PRINCIPAL_TRADER_HIT = "principal_trader_hit";
		public static final String CONSIGNOR_TRADER_SITE_ID = "consignor_trader_site_id";
		public static final String CONSIGNOR_TRADER_NAME = "consignor_trader_name";
		public static final String CONSIGNOR_TRADER_STREET_AND_NUM = "consignor_trader_street_and_num";
		public static final String CONSIGNOR_TRADER_POSTAL_CODE = "consignor_trader_postal_code";
		public static final String CONSIGNOR_TRADER_CITY = "consignor_trader_city";
		public static final String CONSIGNOR_TRADER_COUNTRY_ID = "consignor_trader_country_id";
		public static final String CONSIGNOR_TRADER_COUNTRY_CODE = "consignor_trader_country_code";
		public static final String CONSIGNOR_TRADER_NAD_LNG = "consignor_trader_nad_lng";
		public static final String CONSIGNOR_TRADER_TIN = "consignor_trader_tin";
		public static final String CONSIGNEE_TRADER_SITE_ID = "consignee_trader_site_id";
		public static final String CONSIGNEE_TRADER_NAME = "consignee_trader_name";
		public static final String CONSIGNEE_TRADER_STREET_AND_NUM = "consignee_trader_street_and_num";
		public static final String CONSIGNEE_TRADER_POSTAL_CODE = "consignee_trader_postal_code";
		public static final String CONSIGNEE_TRADER_CITY = "consignee_trader_city";
		public static final String CONSIGNEE_TRADER_COUNTRY_CODE = "consignee_trader_country_code";
		public static final String CONSIGNEE_TRADER_COUNTRY_ID = "consignee_trader_country_id";
		public static final String CONSIGNEE_TRADER_NAD_LNG = "consignee_trader_nad_lng";
		public static final String CONSIGNEE_TRADER_TIN = "consignee_trader_tin";
		public static final String AUTHORISED_CONSIGNEE_SITE_ID = "authorised_consignee_site_id";
		public static final String AUTHORISED_CONSIGNEE_TRAD_TIN = "authorised_consignee_trad_tin";
		public static final String DEPART_CUSTOMS_OFFICE_REF_NUM = "depart_customs_office_ref_num";
		public static final String DEPART_CUSTOMS_OFFICE_ID = "depart_customs_office_id";
		public static final String DEST_CUSTOMS_OFFICE_REF_NUM = "dest_customs_office_ref_num";
		public static final String DEST_CUSTOMS_OFFICE_ID = "dest_customs_office_id";
		public static final String CONTROL_RESULT_CODE = "control_result_code";
		public static final String CONTROL_RESULT_ID = "control_result_id";
		public static final String CONTROL_DATE_LIMIT = "control_date_limit";
		public static final String REPRESENTATIVE_NAME = "representative_name";
		public static final String REPRESENTATIVE_CAPACITY = "representative_capacity";
		public static final String REPRESENTATIVE_CAPACITY_LNG = "representative_capacity_lng";
		public static final String SEALS_NUMBER = "seals_number";
		public static final String CARRIER_SITE_ID = "carrier_site_id";
		public static final String CARRIER_NAME = "carrier_name";
		public static final String CARRIER_STREET_AND_NUM = "carrier_street_and_num";
		public static final String CARRIER_POSTAL_CODE = "carrier_postal_code";
		public static final String CARRIER_CITY = "carrier_city";
		public static final String CARRIER_COUNTRY_CODE = "carrier_country_code";
		public static final String CARRIER_COUNTRY_ID = "carrier_country_id";
		public static final String CARRIER_NAD_LNG = "carrier_nad_lng";
		public static final String CARRIER_TIN = "carrier_tin";
		public static final String SEC_CONSIGNOR_SITE_ID = "sec_consignor_site_id";
		public static final String SEC_CONSIGNOR_NAME = "sec_consignor_name";
		public static final String SEC_CONSIGNOR_STREET_AND_NUM = "sec_consignor_street_and_num";
		public static final String SEC_CONSIGNOR_POSTAL_CODE = "sec_consignor_postal_code";
		public static final String SEC_CONSIGNOR_CITY = "sec_consignor_city";
		public static final String SEC_CONSIGNOR_COUNTRY_CODE = "sec_consignor_country_code";
		public static final String SEC_CONSIGNOR_COUNTRY_ID = "sec_consignor_country_id";
		public static final String SEC_CONSIGNOR_NAD_LNG = "sec_consignor_nad_lng";
		public static final String SEC_CONSIGNOR_TIN = "sec_consignor_tin";
		public static final String SEC_CONSIGNEE_SITE_ID = "sec_consignee_site_id";
		public static final String SEC_CONSIGNEE_NAME = "sec_consignee_name";
		public static final String SEC_CONSIGNEE_STREET_AND_NUM = "sec_consignee_street_and_num";
		public static final String SEC_CONSIGNEE_POSTAL_CODE = "sec_consignee_postal_code";
		public static final String SEC_CONSIGNEE_CITY = "sec_consignee_city";
		public static final String SEC_CONSIGNEE_COUNTRY_CODE = "sec_consignee_country_code";
		public static final String SEC_CONSIGNEE_COUNTRY_ID = "sec_consignee_country_id";
		public static final String SEC_CONSIGNEE_NAD_LNG = "sec_consignee_nad_lng";
		public static final String SEC_CONSIGNEE_TIN = "sec_consignee_tin";
		public static final String ACCEPTANCE_DATE = "acceptance_date";
		public static final String NUMBER_OF_LOADING_LISTS = "number_of_loading_lists";
		public static final String CONTROL_DATE = "control_date";
		public static final String CONTROLLED_BY = "controlled_by";
		public static final String CONTROLLED_BY_LNG = "controlled_by_lng";
		public static final String RETURN_COPY_CO_ID = "return_copy_co_id";
		public static final String RETURN_COPY_CO_REF_NUMBER = "return_copy_co_ref_number";
		public static final String RETURN_COPY_CO_NAME = "return_copy_co_name";
		public static final String RETURN_COPY_CO_STREETAND_NUM = "return_copy_co_streetand_num";
		public static final String RETURN_COPY_CO_COUNTRY_CODE = "return_copy_co_country_code";
		public static final String RETURN_COPY_CO_COUNTRY_ID = "return_copy_co_country_id";
		public static final String RETURN_COPY_CO_POSTAL_CODE = "return_copy_co_postal_code";
		public static final String RETURN_COPY_CO_CITY = "return_copy_co_city";
		public static final String RETURN_COPY_CO_NAD_LNG = "return_copy_co_nad_lng";
		public static final String DATE_OF_CONTROL = "date_of_control";
		public static final String WRITE_OFF_DATE = "write_off_date";
		public static final String GUARANTOR_NAME = "guarantor_name";
		public static final String GUARANTOR_STREET_AND_NUMBER = "guarantor_street_and_number";
		public static final String GUARANTOR_POSTAL_CODE = "guarantor_postal_code";
		public static final String GUARANTOR_CITY = "guarantor_city";
		public static final String GUARANTOR_COUNTRY_CODE = "guarantor_country_code";
		public static final String GUARANTOR_COUNTRY_ID = "guarantor_country_id";
		public static final String GUARANTOR_NAD_LNG = "guarantor_nad_lng";
		public static final String GUARANTOR_TIN = "guarantor_tin";
		public static final String RELEASE_REQUEST_DATE = "release_request_date";
		public static final String STATUS_ID = "status_id";
		public static final String CREATED_DATE = "created_date";
		public static final String CREATOR_USER = "creator_user";
		public static final String MODIFIED_DATE = "modified_date";
		public static final String MODIFIER_USER = "modifier_user";

		private ColumnNames() {
			// intentionally empty
		}
	}

	public static class AttributeNames {

		public static final String NCTS_DECLARATION_ID = "nctsDeclarationId";
		public static final String REFERENCE_NUMBER = "referenceNumber";
		public static final String DOCUMENT_NUMBER = "documentNumber";
		public static final String TYPE_OF_DECLARATION = "typeOfDeclaration";
		public static final String TYPE_OF_DECLARATION_CODE = "typeOfDeclarationCode";
		public static final String TYPE_OF_DECLARATION_ID = "typeOfDeclarationId";
		public static final String COUNTRY_OF_DESTINATION_CODE = "countryOfDestinationCode";
		public static final String COUNTRY_OF_DESTINATION = "countryOfDestination";
		public static final String COUNTRY_OF_DESTINATION_ID = "countryOfDestinationId";
		public static final String AGREED_LOC_OF_GOODS_CODE = "agreedLocOfGoodsCode";
		public static final String AGREED_LOC_OF_GOODS = "agreedLocOfGoods";
		public static final String AGREED_LOC_OF_GOODS_LNG = "agreedLocOfGoodsLng";
		public static final String AUTHORISED_LOC_OF_GOODS_CODE = "authorisedLocOfGoodsCode";
		public static final String PLACE_OF_LOADING = "placeOfLoading";
		public static final String COUNTRY_OF_DISPATCH_CODE = "countryOfDispatchCode";
		public static final String COUNTRY_OF_DISPATCH = "countryOfDispatch";
		public static final String COUNTRY_OF_DISPATCH_ID = "countryOfDispatchId";
		public static final String CUSTOMS_SUB_PLACE = "customsSubPlace";
		public static final String INLAND_TRANSPORT_MODE_CODE = "inlandTransportModeCode";
		public static final String INLAND_TRANSPORT_MODE_ID = "inlandTransportModeId";
		public static final String INLAND_TRANSPORT_MODE = "inlandTransportMode";
		public static final String TRANSPORT_MODE_AT_BORDER_CODE = "transportModeAtBorderCode";
		public static final String TRANSPORT_MODE_AT_BORDER_ID = "transportModeAtBorderId";
		public static final String TRANSPORT_MODE_AT_BORDER = "transportModeAtBorder";
		public static final String IDENT_OF_MEANS_OF_TRANSP_AT_DEP = "identOfMeansOfTranspAtDep";
		public static final String IDENT_OF_MEANS_OF_TRANP_AT_DEP_LNG = "identOfMeansOfTranpAtDepLng";
		public static final String NATION_OF_MEANS_OF_TRANS_AT_DEP_CODE = "nationOfMeansOfTransAtDepCode";
		public static final String NATION_OF_MEANS_OF_TRANS_AT_DEP_ID = "nationOfMeansOfTransAtDepId";
		public static final String NATION_OF_MEANS_OF_TRANS_AT_DEP = "nationOfMeansOfTransAtDep";
		public static final String IDENT_OF_MEANS_OF_TRANS_CROSS_BRD = "identOfMeansOfTransCrossBrd";
		public static final String IDENT_OF_MEANS_OF_TRANS_CR_BRD_LNG = "identOfMeansOfTransCrBrdLng";
		public static final String NATION_OF_MEANS_OF_TRANS_CROSS_BRD_CODE = "nationOfMeansOfTransCrossBrdCode";
		public static final String NATION_OF_MEANS_OF_TRANS_CROSS_BRD_ID = "nationOfMeansOfTransCrossBrdId";
		public static final String NATION_OF_MEANS_OF_TRANS_CROSS_BRD = "nationOfMeansOfTransCrossBrd";
		public static final String TYPE_OF_MEANS_OF_TRANS_CROSS_BRD = "typeOfMeansOfTransCrossBrd";
		public static final String CONTAINER_INDICATOR = "containerIndicator";
		public static final String DIALOG_LNG_INDICATOR_AT_DEP = "dialogLngIndicatorAtDep";
		public static final String NCTS_ACCOMP_DOC_LNG = "nctsAccompDocLng";
		public static final String TOTAL_NUM_OF_ITEMS = "totalNumOfItems";
		public static final String TOTAL_NUM_OF_PACKAGES = "totalNumOfPackages";
		public static final String TOTAL_GROSS_MASS = "totalGrossMass";
		public static final String DECLARATION_DATE = "declarationDate";
		public static final String DECLARATION_PLACE = "declarationPlace";
		public static final String DECLARATION_PLACE_LNG = "declarationPlaceLng";
		public static final String SPECIAL_CIRCUMSTANCE_INDICATOR_CODE = "specialCircumstanceIndicatorCode";
		public static final String SPECIAL_CIRCUMSTANCE_INDICATOR_ID = "specialCircumstanceIndicatorId";
		public static final String SPECIAL_CIRCUMSTANCE_INDICATOR = "specialCircumstanceIndicator";
		public static final String METHOD_OF_PAYMENT_CODE = "methodOfPaymentCode";
		public static final String METHOD_OF_PAYMENT_ID = "methodOfPaymentId";
		public static final String METHOD_OF_PAYMENT = "methodOfPayment";
		public static final String COMMERCIAL_REF_NUM = "commercialRefNum";
		public static final String SEC_DATA = "secData";
		public static final String CON_REFERENCE_NUM = "conReferenceNum";
		public static final String PLACE_OF_UNLOADING = "placeOfUnloading";
		public static final String PLACE_OF_UNLOADING_LNG = "placeOfUnloadingLng";
		public static final String LOCAL_PRINCIPAL_TRADER_SITE = "localPrincipalTraderSite";
		public static final String PRINCIPAL_TRADER_SITE = "principalTraderSite";
		public static final String PRINCIPAL_TRADER_NAME = "principalTraderName";
		public static final String PRINCIPAL_TRADER_STREET_AND_NUM = "principalTraderStreetAndNum";
		public static final String PRINCIPAL_TRADER_POSTAL_CODE = "principalTraderPostalCode";
		public static final String PRINCIPAL_TRADER_CITY = "principalTraderCity";
		public static final String PRINCIPAL_TRADER_COUNTRY_CODE = "principalTraderCountryCode";
		public static final String PRINCIPAL_TRADER_COUNTRY_ID = "principalTraderCountryId";
		public static final String PRINCIPAL_TRADER_COUNTRY = "principalTraderCountry";
		public static final String PRINCIPAL_TRADER_NAD_LNG = "principalTraderNadLng";
		public static final String PRINCIPAL_TRADER_TIN = "principalTraderTin";
		public static final String PRINCIPAL_TRADER_HIT = "principalTraderHit";
		public static final String LOCAL_CONSIGNOR_TRADER_SITE = "localConsignorTraderSite";
		public static final String CONSIGNOR_TRADER_SITE = "consignorTraderSite";
		public static final String CONSIGNOR_TRADER_NAME = "consignorTraderName";
		public static final String CONSIGNOR_TRADER_STREET_AND_NUM = "consignorTraderStreetAndNum";
		public static final String CONSIGNOR_TRADER_POSTAL_CODE = "consignorTraderPostalCode";
		public static final String CONSIGNOR_TRADER_CITY = "consignorTraderCity";
		public static final String CONSIGNOR_TRADER_COUNTRY_CODE = "consignorTraderCountryCode";
		public static final String CONSIGNOR_TRADER_COUNTRY = "consignorTraderCountry";
		public static final String CONSIGNOR_TRADER_COUNTRY_ID = "consignorTraderCountryId";
		public static final String CONSIGNOR_TRADER_NAD_LNG = "consignorTraderNadLng";
		public static final String CONSIGNOR_TRADER_TIN = "consignorTraderTin";
		public static final String LOCAL_CONSIGNEE_TRADER_SITE = "localConsigneeTraderSite";
		public static final String CONSIGNEE_TRADER_SITE = "consigneeTraderSite";
		public static final String CONSIGNEE_TRADER_NAME = "consigneeTraderName";
		public static final String CONSIGNEE_TRADER_STREET_AND_NUM = "consigneeTraderStreetAndNum";
		public static final String CONSIGNEE_TRADER_POSTAL_CODE = "consigneeTraderPostalCode";
		public static final String CONSIGNEE_TRADER_CITY = "consigneeTraderCity";
		public static final String CONSIGNEE_TRADER_COUNTRY_CODE = "consigneeTraderCountryCode";
		public static final String CONSIGNEE_TRADER_COUNTRY = "consigneeTraderCountry";
		public static final String CONSIGNEE_TRADER_COUNTRY_ID = "consigneeTraderCountryId";
		public static final String CONSIGNEE_TRADER_NAD_LNG = "consigneeTraderNadLng";
		public static final String CONSIGNEE_TRADER_TIN = "consigneeTraderTin";
		public static final String AUTHORISED_CONSIGNEE_SITE = "authorisedConsigneeSite";
		public static final String AUTHORISED_CONSIGNEE_TRAD_TIN = "authorisedConsigneeTradTin";
		public static final String DEPART_CUSTOMS_OFFICE = "departCustomsOffice";
		public static final String DEPART_CUSTOMS_OFFICE_REF_NUM = "departCustomsOfficeRefNum";
		public static final String DEPART_CUSTOMS_OFFICE_ID = "departCustomsOfficeId";
		public static final String DEST_CUSTOMS_OFFICE = "destCustomsOffice";
		public static final String DEST_CUSTOMS_OFFICE_REF_NUM = "destCustomsOfficeRefNum";
		public static final String DEST_CUSTOMS_OFFICE_ID = "destCustomsOfficeRefNumId";
		public static final String CONTROL_RESULT_CODE = "controlResultCode";
		public static final String CONTROL_RESULT = "controlResult";
		public static final String CONTROL_RESULT_ID = "controlResultId";
		public static final String CONTROL_DATE_LIMIT = "controlDateLimit";
		public static final String REPRESENTATIVE_NAME = "representativeName";
		public static final String REPRESENTATIVE_CAPACITY = "representativeCapacity";
		public static final String REPRESENTATIVE_CAPACITY_LNG = "representativeCapacityLng";
		public static final String SEALS_NUMBER = "sealsNumber";
		public static final String LOCAL_CARRIER_SITE = "localCarrierSite";
		public static final String CARRIER_SITE_ID = "carrierSiteId";
		public static final String CARRIER_SITE = "carrierSite";
		public static final String CARRIER_NAME = "carrierName";
		public static final String CARRIER_STREET_AND_NUM = "carrierStreetAndNum";
		public static final String CARRIER_POSTAL_CODE = "carrierPostalCode";
		public static final String CARRIER_CITY = "carrierCity";
		public static final String CARRIER_COUNTRY_CODE = "carrierCountryCode";
		public static final String CARRIER_COUNTRY = "carrierCountry";
		public static final String CARRIER_COUNTRY_ID = "carrierCountryId";
		public static final String CARRIER_NAD_LNG = "carrierNadLng";
		public static final String CARRIER_TIN = "carrierTin";
		public static final String LOCAL_SEC_CONSIGNOR_SITE = "localSecConsignorSite";
		public static final String SEC_CONSIGNOR_SITE = "secConsignorSite";
		public static final String SEC_CONSIGNOR_SITE_ID = "secConsignorSiteId";
		public static final String SEC_CONSIGNOR_NAME = "secConsignorName";
		public static final String SEC_CONSIGNOR_STREET_AND_NUM = "secConsignorStreetAndNum";
		public static final String SEC_CONSIGNOR_POSTAL_CODE = "secConsignorPostalCode";
		public static final String SEC_CONSIGNOR_CITY = "secConsignorCity";
		public static final String SEC_CONSIGNOR_COUNTRY_CODE = "secConsignorCountryCode";
		public static final String SEC_CONSIGNOR_COUNTRY = "secConsignorCountry";
		public static final String SEC_CONSIGNOR_COUNTRY_ID = "secConsignorCountryId";
		public static final String SEC_CONSIGNOR_NAD_LNG = "secConsignorNadLng";
		public static final String SEC_CONSIGNOR_TIN = "secConsignorTin";
		public static final String LOCAL_SEC_CONSIGNEE_SITE = "localSecConsigneeSite";
		public static final String SEC_CONSIGNEE_SITE_ID = "secConsigneeSiteId";
		public static final String SEC_CONSIGNEE_SITE = "secConsigneeSite";
		public static final String SEC_CONSIGNEE_NAME = "secConsigneeName";
		public static final String SEC_CONSIGNEE_STREET_AND_NUM = "secConsigneeStreetAndNum";
		public static final String SEC_CONSIGNEE_POSTAL_CODE = "secConsigneePostalCode";
		public static final String SEC_CONSIGNEE_CITY = "secConsigneeCity";
		public static final String SEC_CONSIGNEE_COUNTRY_CODE = "secConsigneeCountryCode";
		public static final String SEC_CONSIGNEE_COUNTRY = "secConsigneeCountry";
		public static final String SEC_CONSIGNEE_COUNTRY_ID = "secConsigneeCountryId";
		public static final String SEC_CONSIGNEE_NAD_LNG = "secConsigneeNadLng";
		public static final String SEC_CONSIGNEE_TIN = "secConsigneeTin";
		public static final String ACCEPTANCE_DATE = "acceptanceDate";
		public static final String NUMBER_OF_LOADING_LISTS = "numberOfLoadingLists";
		public static final String CONTROL_DATE = "controlDate";
		public static final String CONTROLLED_BY = "controlledBy";
		public static final String CONTROLLED_BY_LNG = "controlledByLng";
		public static final String RETURN_COPY_CO_ID = "returnCopyCoId";
		public static final String RETURN_COPY_CO_REF_NUMBER = "returnCopyCoRefNumber";
		public static final String RETURN_COPY_CO_NAME = "returnCopyCoName";
		public static final String RETURN_COPY_CO_STREETAND_NUM = "returnCopyCoStreetandNum";
		public static final String RETURN_COPY_CO_COUNTRY_CODE = "returnCopyCoCountryCode";
		public static final String RETURN_COPY_CO_COUNTRY = "returnCopyCoCountry";
		public static final String RETURN_COPY_CO_COUNTRY_ID = "returnCopyCoCountryId";
		public static final String RETURN_COPY_CO_POSTAL_CODE = "returnCopyCoPostalCode";
		public static final String RETURN_COPY_CO_CITY = "returnCopyCoCity";
		public static final String RETURN_COPY_CO_NAD_LNG = "returnCopyCoNadLng";
		public static final String DATE_OF_CONTROL = "dateOfControl";
		public static final String WRITE_OFF_DATE = "writeOffDate";
		public static final String GUARANTOR_NAME = "guarantorName";
		public static final String GUARANTOR_STREET_AND_NUMBER = "guarantorStreetAndNumber";
		public static final String GUARANTOR_POSTAL_CODE = "guarantorPostalCode";
		public static final String GUARANTOR_CITY = "guarantorCity";
		public static final String GUARANTOR_COUNTRY_CODE = "guarantorCountryCode";
		public static final String GUARANTOR_COUNTRY = "guarantorCountry";
		public static final String GUARANTOR_COUNTRY_ID = "guarantorCountryId";
		public static final String GUARANTOR_NAD_LNG = "guarantorNadLng";
		public static final String GUARANTOR_TIN = "guarantorTin";
		public static final String RELEASE_REQUEST_DATE = "releaseRequestDate";
		public static final String STATUS = "status";
		public static final String STATUS_ID = "statusId";
		public static final String CREATED_DATE = "createdDate";
		public static final String CREATOR_USER = "creatorUser";
		public static final String MODIFIED_DATE = "modifiedDate";
		public static final String MODIFIER_USER = "modifierUser";
		public static final String NCTS_DECLARATION_ITEMS = "nctsDeclarationItems";
		public static final String NCTS_TRANSITION_CUSTOMS_OFFICES = "transitionCustomsOffices";
		public static final String NCTS_SEALS = "seals";
		public static final String GUARANTEES = "guarantees";
		public static final String REJECTIONS = "rejections";

		private AttributeNames() {
			// intentionally empty
		}
	}

	private Long nctsDeclarationId;
	private String referenceNumber;
	private String documentNumber;
	private String typeOfDeclarationCode;
	private MasterData typeOfDeclaration;
	private Long typeOfDeclarationId;
	private String countryOfDestinationCode;
	private Country countryOfDestination;
	private Long countryOfDestinationId;
	private String agreedLocOfGoodsCode;
	private String agreedLocOfGoods;
	private String agreedLocOfGoodsLng;
	private String authorisedLocOfGoodsCode;
	private String placeOfLoading;
	private String countryOfDispatchCode;
	private Country countryOfDispatch;
	private Long countryOfDispatchId;
	private String customsSubPlace;
	private String inlandTransportModeCode;
	private String transportModeAtBorderCode;
	private MasterData inlandTransportMode;
	private Long inlandTransportModeId;
	private MasterData transportModeAtBorder;
	private Long transportModeAtBorderId;
	private String identOfMeansOfTranspAtDep;
	private String identOfMeansOfTranpAtDepLng;
	private Long nationOfMeansOfTransAtDepId;
	private Country nationOfMeansOfTransAtDep;
	private String nationOfMeansOfTransAtDepCode;
	private String identOfMeansOfTransCrossBrd;
	private String identOfMeansOfTransCrBrdLng;
	private Country nationOfMeansOfTransCrossBrd;
	private Long nationOfMeansOfTransCrossBrdId;
	private String nationOfMeansOfTransCrossBrdCode;
	private Integer typeOfMeansOfTransCrossBrd;
	private boolean containerIndicator;
	private String dialogLngIndicatorAtDep;
	private String nctsAccompDocLng;
	private Integer totalNumOfItems;
	private Integer totalNumOfPackages;
	private Double totalGrossMass;
	private Date declarationDate;
	private String declarationPlace;
	private String declarationPlaceLng;
	private MasterData specialCircumstanceIndicator;
	private Long specialCircumstanceIndicatorId;
	private String specialCircumstanceIndicatorCode;
	private MasterData methodOfPayment;
	private Long methodOfPaymentId;
	private String methodOfPaymentCode;
	private String commercialRefNum;
	private boolean secData;
	private String conReferenceNum;
	private String placeOfUnloading;
	private String placeOfUnloadingLng;
	private PartnerSite principalTraderSite;
	private Long principalTraderSiteId;
	private LocalPrincipalTraderSite localPrincipalTrader;
	private PartnerSite consignorTraderSite;
	private Long consignorTraderSiteId;
	private LocalConsignorTraderSite localConsignorTraderSite;
	private PartnerSite consigneeTraderSite;
	private Long consigneeTraderSiteId;
	private LocalConsigneeTraderSite localConsigneeTraderSite;
	private PartnerSite authorisedConsigneeSite;
	private Long authorisedConsigneeSiteId;
	private String authorisedConsigneeTradTin;
	private String departCustomsOfficeRefNum;
	private CustomsOffice departCustomsOffice;
	private Long departCustomsOfficeId;
	private String destCustomsOfficeRefNum;
	private CustomsOffice destCustomsOffice;
	private Long destCustomsOfficeId;
	private MasterData controlResult;
	private Long controlResultId;
	private String controlResultCode;
	private Date controlDateLimit;
	private String representativeName;
	private String representativeCapacity;
	private String representativeCapacityLng;
	private Integer sealsNumber;
	private PartnerSite carrierSite;
	private Long carrierSiteId;
	private LocalCarrierSite localCarrierSite;
	private PartnerSite secConsignorSite;
	private Long secConsignorSiteId;
	private LocalSecConsignorSite localSecConsignorSite;
	private PartnerSite secConsigneeSite;
	private Long secConsigneeSiteId;
	private LocalSecConsigneeSite localSecConsigneeSite;
	private Date acceptanceDate;
	private Integer numberOfLoadingLists;
	private Date controlDate;
	private String controlledBy;
	private String controlledByLng;
	private LocalReturnCopyCo localReturnCopyCo;
	private CustomsOffice returnCopyCoCustomsOffice;
	private Long returnCopyCoCustomsOfficeId;
	private Date dateOfControl;
	private Date writeOffDate;
	private LocalGuarantorSite localGuarantorSite;
	private Date releaseRequestDate;
	private NCTSDeclStatus status;
	private Long statusId;
	private Date createdDate;
	private User creatorUser;
	private Date modifiedDate;
	private User modifierUser;
	private List<NCTSDeclItem> nctsDeclarationItems;
	private List<NCTSDeclTransitCustomsOffice> transitionCustomsOffices;
	private List<NCTSDeclSeal> seals;
	private List<NCTSDeclGuarantee> guarantees;
	private List<NCTSDeclRejection> rejections;
	private List<NCTSDeclGuaranteeInvalid> invalidGuarantees;
	private List<NCTSDeclRoute> routes;

	@Id
	@GeneratedValue
	@Column(name = ColumnNames.NCTS_DECLARATION_ID, nullable = false)
	public Long getNctsDeclarationId() {
		return nctsDeclarationId;
	}

	public void setNctsDeclarationId(Long nctsDeclarationId) {
		this.nctsDeclarationId = nctsDeclarationId;
	}

	@Column(name = ColumnNames.REFERENCE_NUMBER, length = 22)
	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	@Column(name = ColumnNames.DOCUMENT_NUMBER, length = 21)
	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	@ManyToOne
	@JoinColumn(name = ColumnNames.TYPE_OF_DECLARATION_ID)
	public MasterData getTypeOfDeclaration() {
		return typeOfDeclaration;
	}

	public void setTypeOfDeclaration(MasterData typeOfDeclaration) {
		this.typeOfDeclaration = typeOfDeclaration;
	}

	@Column(name = ColumnNames.TYPE_OF_DECLARATION_CODE, length = 9)
	public String getTypeOfDeclarationCode() {
		return typeOfDeclarationCode;
	}

	public void setTypeOfDeclarationCode(String typeOfDeclarationCode) {
		this.typeOfDeclarationCode = typeOfDeclarationCode;
	}

	@Column(name = ColumnNames.TYPE_OF_DECLARATION_ID, insertable = false, updatable = false)
	public Long getTypeOfDeclarationId() {
		return typeOfDeclarationId;
	}

	public void setTypeOfDeclarationId(Long typeOfDeclarationId) {
		this.typeOfDeclarationId = typeOfDeclarationId;
	}

	@ManyToOne
	@JoinColumn(name = ColumnNames.COUNTRY_OF_DESTINATION_ID)
	public Country getCountryOfDestination() {
		return countryOfDestination;
	}

	public void setCountryOfDestination(Country countryOfDestination) {
		this.countryOfDestination = countryOfDestination;
	}

	@Column(name = ColumnNames.COUNTRY_OF_DESTINATION_ID, insertable = false, updatable = false)
	public Long getCountryOfDestinationId() {
		return countryOfDestinationId;
	}

	public void setCountryOfDestinationId(Long countryOfDestinationId) {
		this.countryOfDestinationId = countryOfDestinationId;
	}

	@Column(name = ColumnNames.COUNTRY_OF_DESTINATION_CODE, length = 2)
	public String getCountryOfDestinationCode() {
		return countryOfDestinationCode;
	}

	public void setCountryOfDestinationCode(String countryOfDestinationCode) {
		this.countryOfDestinationCode = countryOfDestinationCode;
	}

	@Column(name = ColumnNames.AGREED_LOC_OF_GOODS_CODE, length = 17)
	public String getAgreedLocOfGoodsCode() {
		return agreedLocOfGoodsCode;
	}

	public void setAgreedLocOfGoodsCode(String agreedLocOfGoodsCode) {
		this.agreedLocOfGoodsCode = agreedLocOfGoodsCode;
	}

	@Column(name = ColumnNames.AGREED_LOC_OF_GOODS, length = 35)
	public String getAgreedLocOfGoods() {
		return agreedLocOfGoods;
	}

	public void setAgreedLocOfGoods(String agreedLocOfGoods) {
		this.agreedLocOfGoods = agreedLocOfGoods;
	}

	@Column(name = ColumnNames.AGREED_LOC_OF_GOODS_LNG, length = 2)
	public String getAgreedLocOfGoodsLng() {
		return agreedLocOfGoodsLng;
	}

	public void setAgreedLocOfGoodsLng(String agreedLocOfGoodsLng) {
		this.agreedLocOfGoodsLng = agreedLocOfGoodsLng;
	}

	@Column(name = ColumnNames.AUTHORISED_LOC_OF_GOODS_CODE, length = 17)
	public String getAuthorisedLocOfGoodsCode() {
		return authorisedLocOfGoodsCode;
	}

	public void setAuthorisedLocOfGoodsCode(String authorisedLocOfGoodsCode) {
		this.authorisedLocOfGoodsCode = authorisedLocOfGoodsCode;
	}

	@Column(name = ColumnNames.PLACE_OF_LOADING, length = 17)
	public String getPlaceOfLoading() {
		return placeOfLoading;
	}

	public void setPlaceOfLoading(String placeOfLoading) {
		this.placeOfLoading = placeOfLoading;
	}

	@Column(name = ColumnNames.COUNTRY_OF_DISPATCH_CODE, length = 2)
	public String getCountryOfDispatchCode() {
		return countryOfDispatchCode;
	}

	public void setCountryOfDispatchCode(String countryOfDispatchCode) {
		this.countryOfDispatchCode = countryOfDispatchCode;
	}

	@ManyToOne
	@JoinColumn(name = ColumnNames.COUNTRY_OF_DISPATCH_ID)
	public Country getCountryOfDispatch() {
		return countryOfDispatch;
	}

	public void setCountryOfDispatch(Country countryOfDispatch) {
		this.countryOfDispatch = countryOfDispatch;
	}

	@Column(name = ColumnNames.COUNTRY_OF_DISPATCH_ID, insertable = false, updatable = false)
	public Long getCountryOfDispatchId() {
		return countryOfDispatchId;
	}

	public void setCountryOfDispatchId(Long countryOfDispatchId) {
		this.countryOfDispatchId = countryOfDispatchId;
	}

	@Column(name = ColumnNames.CUSTOMS_SUB_PLACE, length = 17)
	public String getCustomsSubPlace() {
		return customsSubPlace;
	}

	public void setCustomsSubPlace(String customsSubPlace) {
		this.customsSubPlace = customsSubPlace;
	}

	@ManyToOne
	@JoinColumn(name = ColumnNames.INLAND_TRANSPORT_MODE_ID)
	public MasterData getInlandTransportMode() {
		return inlandTransportMode;
	}

	public void setInlandTransportMode(MasterData inlandTransportMode) {
		this.inlandTransportMode = inlandTransportMode;
	}

	@Column(name = ColumnNames.INLAND_TRANSPORT_MODE_CODE)
	public String getInlandTransportModeCode() {
		return inlandTransportModeCode;
	}

	public void setInlandTransportModeCode(String inlandTransportModeCode) {
		this.inlandTransportModeCode = inlandTransportModeCode;
	}

	@Column(name = ColumnNames.INLAND_TRANSPORT_MODE_ID, insertable = false, updatable = false)
	public Long getInlandTransportModeId() {
		return inlandTransportModeId;
	}

	public void setInlandTransportModeId(Long inlandTransportModeId) {
		this.inlandTransportModeId = inlandTransportModeId;
	}

	@Column(name = ColumnNames.TRANSPORT_MODE_AT_BORDER_CODE)
	public String getTransportModeAtBorderCode() {
		return transportModeAtBorderCode;
	}

	public void setTransportModeAtBorderCode(String transportModeAtBorderCode) {
		this.transportModeAtBorderCode = transportModeAtBorderCode;
	}

	@Column(name = ColumnNames.TRANSPORT_MODE_AT_BORDER_ID, insertable = false, updatable = false)
	public Long getTransportModeAtBorderId() {
		return transportModeAtBorderId;
	}

	public void setTransportModeAtBorderId(Long transportModeAtBorderId) {
		this.transportModeAtBorderId = transportModeAtBorderId;
	}

	@ManyToOne
	@JoinColumn(name = ColumnNames.TRANSPORT_MODE_AT_BORDER_ID)
	public MasterData getTransportModeAtBorder() {
		return transportModeAtBorder;
	}

	public void setTransportModeAtBorder(MasterData transportModeAtBorder) {
		this.transportModeAtBorder = transportModeAtBorder;
	}

	@Column(name = ColumnNames.IDENT_OF_MEANS_OF_TRANSP_AT_DEP, length = 27)
	public String getIdentOfMeansOfTranspAtDep() {
		return identOfMeansOfTranspAtDep;
	}

	public void setIdentOfMeansOfTranspAtDep(String identOfMeansOfTranspAtDep) {
		this.identOfMeansOfTranspAtDep = identOfMeansOfTranspAtDep;
	}

	@Column(name = ColumnNames.IDENT_OF_MEANS_OF_TRANP_AT_DEP_LNG, length = 2)
	public String getIdentOfMeansOfTranpAtDepLng() {
		return identOfMeansOfTranpAtDepLng;
	}

	public void setIdentOfMeansOfTranpAtDepLng(String identOfMeansOfTranpAtDepLng) {
		this.identOfMeansOfTranpAtDepLng = identOfMeansOfTranpAtDepLng;
	}

	@Column(name = ColumnNames.NATION_OF_MEANS_OF_TRANS_AT_DEP_ID, insertable = false, updatable = false)
	public Long getNationOfMeansOfTransAtDepId() {
		return nationOfMeansOfTransAtDepId;
	}

	public void setNationOfMeansOfTransAtDepId(Long nationOfMeansOfTransAtDepId) {
		this.nationOfMeansOfTransAtDepId = nationOfMeansOfTransAtDepId;
	}

	@ManyToOne
	@JoinColumn(name = ColumnNames.NATION_OF_MEANS_OF_TRANS_AT_DEP_ID)
	public Country getNationOfMeansOfTransAtDep() {
		return nationOfMeansOfTransAtDep;
	}

	public void setNationOfMeansOfTransAtDep(Country nationOfMeansOfTransAtDep) {
		this.nationOfMeansOfTransAtDep = nationOfMeansOfTransAtDep;
	}

	@Column(name = ColumnNames.NATION_OF_MEANS_OF_TRANS_AT_DEP_CODE, length = 2)
	public String getNationOfMeansOfTransAtDepCode() {
		return nationOfMeansOfTransAtDepCode;
	}

	public void setNationOfMeansOfTransAtDepCode(String nationOfMeansOfTransAtDepCode) {
		this.nationOfMeansOfTransAtDepCode = nationOfMeansOfTransAtDepCode;
	}

	@Column(name = ColumnNames.IDENT_OF_MEANS_OF_TRANS_CROSS_BRD, length = 27)
	public String getIdentOfMeansOfTransCrossBrd() {
		return identOfMeansOfTransCrossBrd;
	}

	public void setIdentOfMeansOfTransCrossBrd(String identOfMeansOfTransCrossBrd) {
		this.identOfMeansOfTransCrossBrd = identOfMeansOfTransCrossBrd;
	}

	@Column(name = ColumnNames.IDENT_OF_MEANS_OF_TRANS_CR_BRD_LNG, length = 2)
	public String getIdentOfMeansOfTransCrBrdLng() {
		return identOfMeansOfTransCrBrdLng;
	}

	public void setIdentOfMeansOfTransCrBrdLng(String identOfMeansOfTransCrBrdLng) {
		this.identOfMeansOfTransCrBrdLng = identOfMeansOfTransCrBrdLng;
	}

	@ManyToOne
	@JoinColumn(name = ColumnNames.NATION_OF_MEANS_OF_TRANS_CROSS_BRD_ID)
	public Country getNationOfMeansOfTransCrossBrd() {
		return nationOfMeansOfTransCrossBrd;
	}

	@Column(name = ColumnNames.NATION_OF_MEANS_OF_TRANS_CROSS_BRD_ID, insertable = false, updatable = false)
	public Long getNationOfMeansOfTransCrossBrdId() {
		return nationOfMeansOfTransCrossBrdId;
	}

	public void setNationOfMeansOfTransCrossBrdId(Long nationOfMeansOfTransCrossBrdId) {
		this.nationOfMeansOfTransCrossBrdId = nationOfMeansOfTransCrossBrdId;
	}

	@Column(name = ColumnNames.NATION_OF_MEANS_OF_TRANS_CROSS_BRD_CODE, length = 2)
	public String getNationOfMeansOfTransCrossBrdCode() {
		return nationOfMeansOfTransCrossBrdCode;
	}

	public void setNationOfMeansOfTransCrossBrdCode(String nationOfMeansOfTransCrossBrdCode) {
		this.nationOfMeansOfTransCrossBrdCode = nationOfMeansOfTransCrossBrdCode;
	}

	public void setNationOfMeansOfTransCrossBrd(Country nationOfMeansOfTransCrossBrd) {
		this.nationOfMeansOfTransCrossBrd = nationOfMeansOfTransCrossBrd;
	}

	@Column(name = ColumnNames.TYPE_OF_MEANS_OF_TRANS_CROSS_BRD)
	public Integer getTypeOfMeansOfTransCrossBrd() {
		return typeOfMeansOfTransCrossBrd;
	}

	public void setTypeOfMeansOfTransCrossBrd(Integer typeOfMeansOfTransCrossBrd) {
		this.typeOfMeansOfTransCrossBrd = typeOfMeansOfTransCrossBrd;
	}

	@Column(name = ColumnNames.CONTAINER_INDICATOR)
	public boolean isContainerIndicator() {
		return containerIndicator;
	}

	public void setContainerIndicator(boolean containerIndicator) {
		this.containerIndicator = containerIndicator;
	}

	@Column(name = ColumnNames.DIALOG_LNG_INDICATOR_AT_DEP, length = 2)
	public String getDialogLngIndicatorAtDep() {
		return dialogLngIndicatorAtDep;
	}

	public void setDialogLngIndicatorAtDep(String dialogLngIndicatorAtDep) {
		this.dialogLngIndicatorAtDep = dialogLngIndicatorAtDep;
	}

	@Column(name = ColumnNames.NCTS_ACCOMP_DOC_LNG, length = 2)
	public String getNctsAccompDocLng() {
		return nctsAccompDocLng;
	}

	public void setNctsAccompDocLng(String nctsAccompDocLng) {
		this.nctsAccompDocLng = nctsAccompDocLng;
	}

	@Column(name = ColumnNames.TOTAL_NUM_OF_ITEMS)
	public Integer getTotalNumOfItems() {
		return totalNumOfItems;
	}

	public void setTotalNumOfItems(Integer totalNumOfItems) {
		this.totalNumOfItems = totalNumOfItems;
	}

	@Column(name = ColumnNames.TOTAL_NUM_OF_PACKAGES)
	public Integer getTotalNumOfPackages() {
		return totalNumOfPackages;
	}

	public void setTotalNumOfPackages(Integer totalNumOfPackages) {
		this.totalNumOfPackages = totalNumOfPackages;
	}

	@Column(name = ColumnNames.TOTAL_GROSS_MASS, precision = 11, scale = 3)
	public Double getTotalGrossMass() {
		return totalGrossMass;
	}

	public void setTotalGrossMass(Double totalGrossMass) {
		this.totalGrossMass = totalGrossMass;
	}

	@Column(name = ColumnNames.DECLARATION_DATE)
	public Date getDeclarationDate() {
		return declarationDate;
	}

	public void setDeclarationDate(Date declarationDate) {
		this.declarationDate = declarationDate;
	}

	@Column(name = ColumnNames.DECLARATION_PLACE, length = 35)
	public String getDeclarationPlace() {
		return declarationPlace;
	}

	public void setDeclarationPlace(String declarationPlace) {
		this.declarationPlace = declarationPlace;
	}

	@Column(name = ColumnNames.DECLARATION_PLACE_LNG, length = 2)
	public String getDeclarationPlaceLng() {
		return declarationPlaceLng;
	}

	public void setDeclarationPlaceLng(String declarationPlaceLng) {
		this.declarationPlaceLng = declarationPlaceLng;
	}

	@ManyToOne
	@JoinColumn(name = ColumnNames.SPECIAL_CIRCUMSTANCE_INDICATOR_ID)
	public MasterData getSpecialCircumstanceIndicator() {
		return specialCircumstanceIndicator;
	}

	public void setSpecialCircumstanceIndicator(MasterData specialCircumstanceIndicator) {
		this.specialCircumstanceIndicator = specialCircumstanceIndicator;
	}

	@Column(name = ColumnNames.SPECIAL_CIRCUMSTANCE_INDICATOR_ID, insertable = false, updatable = false)
	public Long getSpecialCircumstanceIndicatorId() {
		return specialCircumstanceIndicatorId;
	}

	public void setSpecialCircumstanceIndicatorId(Long specialCircumstanceIndicatorId) {
		this.specialCircumstanceIndicatorId = specialCircumstanceIndicatorId;
	}

	@Column(name = ColumnNames.SPECIAL_CIRCUMSTANCE_INDICATOR_CODE, length = 1)
	public String getSpecialCircumstanceIndicatorCode() {
		return specialCircumstanceIndicatorCode;
	}

	public void setSpecialCircumstanceIndicatorCode(String specialCircumstanceIndicatorCode) {
		this.specialCircumstanceIndicatorCode = specialCircumstanceIndicatorCode;
	}

	@ManyToOne
	@JoinColumn(name = ColumnNames.METHOD_OF_PAYMENT_ID)
	public MasterData getMethodOfPayment() {
		return methodOfPayment;
	}

	public void setMethodOfPayment(MasterData methodOfPayment) {
		this.methodOfPayment = methodOfPayment;
	}

	@Column(name = ColumnNames.METHOD_OF_PAYMENT_ID, insertable = false, updatable = false)
	public Long getMethodOfPaymentId() {
		return methodOfPaymentId;
	}

	public void setMethodOfPaymentId(Long methodOfPaymentId) {
		this.methodOfPaymentId = methodOfPaymentId;
	}

	@Column(name = ColumnNames.METHOD_OF_PAYMENT_CODE, length = 1)
	public String getMethodOfPaymentCode() {
		return methodOfPaymentCode;
	}

	public void setMethodOfPaymentCode(String methodOfPaymentCode) {
		this.methodOfPaymentCode = methodOfPaymentCode;
	}

	@Column(name = ColumnNames.COMMERCIAL_REF_NUM, length = 70)
	public String getCommercialRefNum() {
		return commercialRefNum;
	}

	public void setCommercialRefNum(String commercialRefNum) {
		this.commercialRefNum = commercialRefNum;
	}

	@Column(name = ColumnNames.SEC_DATA)
	public boolean isSecData() {
		return secData;
	}

	public void setSecData(boolean secData) {
		this.secData = secData;
	}

	@Column(name = ColumnNames.CON_REFERENCE_NUM, length = 35)
	public String getConReferenceNum() {
		return conReferenceNum;
	}

	public void setConReferenceNum(String conReferenceNum) {
		this.conReferenceNum = conReferenceNum;
	}

	@Column(name = ColumnNames.PLACE_OF_UNLOADING, length = 35)
	public String getPlaceOfUnloading() {
		return placeOfUnloading;
	}

	public void setPlaceOfUnloading(String placeOfUnloading) {
		this.placeOfUnloading = placeOfUnloading;
	}

	@Column(name = ColumnNames.PLACE_OF_UNLOADING_LNG, length = 2)
	public String getPlaceOfUnloadingLng() {
		return placeOfUnloadingLng;
	}

	public void setPlaceOfUnloadingLng(String placeOfUnloadingLng) {
		this.placeOfUnloadingLng = placeOfUnloadingLng;
	}

	@ManyToOne
	@JoinColumn(name = ColumnNames.PRINCIPAL_TRADER_SITE_ID)
	public PartnerSite getPrincipalTraderSite() {
		return principalTraderSite;
	}

	public void setPrincipalTraderSite(PartnerSite principalTraderSite) {
		this.principalTraderSite = principalTraderSite;
	}

	@Column(name = ColumnNames.PRINCIPAL_TRADER_SITE_ID, insertable = false, updatable = false)
	public Long getPrincipalTraderSiteId() {
		return principalTraderSiteId;
	}

	public void setPrincipalTraderSiteId(Long principalTraderSiteId) {
		this.principalTraderSiteId = principalTraderSiteId;
	}

	@Embedded
	public LocalPrincipalTraderSite getLocalPrincipalTrader() {
		return localPrincipalTrader;
	}

	public void setLocalPrincipalTrader(LocalPrincipalTraderSite localPrincipalTrader) {
		this.localPrincipalTrader = localPrincipalTrader;
	}

	@ManyToOne
	@JoinColumn(name = ColumnNames.CONSIGNOR_TRADER_SITE_ID)
	public PartnerSite getConsignorTraderSite() {
		return consignorTraderSite;
	}

	public void setConsignorTraderSite(PartnerSite consignorTraderSite) {
		this.consignorTraderSite = consignorTraderSite;
	}

	@Column(name = ColumnNames.CONSIGNOR_TRADER_SITE_ID, insertable = false, updatable = false)
	public Long getConsignorTraderSiteId() {
		return consignorTraderSiteId;
	}

	public void setConsignorTraderSiteId(Long consignorTraderSiteId) {
		this.consignorTraderSiteId = consignorTraderSiteId;
	}

	@Embedded
	public LocalConsignorTraderSite getLocalConsignorTraderSite() {
		return localConsignorTraderSite;
	}

	public void setLocalConsignorTraderSite(LocalConsignorTraderSite localConsignorTraderSite) {
		this.localConsignorTraderSite = localConsignorTraderSite;
	}

	@ManyToOne
	@JoinColumn(name = ColumnNames.CONSIGNEE_TRADER_SITE_ID)
	public PartnerSite getConsigneeTraderSite() {
		return consigneeTraderSite;
	}

	public void setConsigneeTraderSite(PartnerSite consigneeTraderSite) {
		this.consigneeTraderSite = consigneeTraderSite;
	}

	@Column(name = ColumnNames.CONSIGNEE_TRADER_SITE_ID, insertable = false, updatable = false)
	public Long getConsigneeTraderSiteId() {
		return consigneeTraderSiteId;
	}

	public void setConsigneeTraderSiteId(Long consigneeTraderSiteId) {
		this.consigneeTraderSiteId = consigneeTraderSiteId;
	}

	@Embedded
	public LocalConsigneeTraderSite getLocalConsigneeTraderSite() {
		return localConsigneeTraderSite;
	}

	public void setLocalConsigneeTraderSite(LocalConsigneeTraderSite localConsigneeTraderSite) {
		this.localConsigneeTraderSite = localConsigneeTraderSite;
	}

	@ManyToOne
	@JoinColumn(name = ColumnNames.AUTHORISED_CONSIGNEE_SITE_ID)
	public PartnerSite getAuthorisedConsigneeSite() {
		return authorisedConsigneeSite;
	}

	public void setAuthorisedConsigneeSite(PartnerSite authorisedConsigneeSite) {
		this.authorisedConsigneeSite = authorisedConsigneeSite;
	}

	@Column(name = ColumnNames.AUTHORISED_CONSIGNEE_SITE_ID, insertable = false, updatable = false)
	public Long getAuthorisedConsigneeSiteId() {
		return authorisedConsigneeSiteId;
	}

	public void setAuthorisedConsigneeSiteId(Long authorisedConsigneeSiteId) {
		this.authorisedConsigneeSiteId = authorisedConsigneeSiteId;
	}

	@Column(name = ColumnNames.AUTHORISED_CONSIGNEE_TRAD_TIN, length = 17)
	public String getAuthorisedConsigneeTradTin() {
		return authorisedConsigneeTradTin;
	}

	public void setAuthorisedConsigneeTradTin(String authorisedConsigneeTradTin) {
		this.authorisedConsigneeTradTin = authorisedConsigneeTradTin;
	}

	@Column(name = ColumnNames.DEPART_CUSTOMS_OFFICE_REF_NUM, length = 8)
	public String getDepartCustomsOfficeRefNum() {
		return departCustomsOfficeRefNum;
	}

	public void setDepartCustomsOfficeRefNum(String departCustomsOfficeRefNum) {
		this.departCustomsOfficeRefNum = departCustomsOfficeRefNum;
	}

	@ManyToOne
	@JoinColumn(name = ColumnNames.DEPART_CUSTOMS_OFFICE_ID)
	public CustomsOffice getDepartCustomsOffice() {
		return departCustomsOffice;
	}

	public void setDepartCustomsOffice(CustomsOffice departCustomsOffice) {
		this.departCustomsOffice = departCustomsOffice;
	}

	@Column(name = ColumnNames.DEPART_CUSTOMS_OFFICE_ID, insertable = false, updatable = false)
	public Long getDepartCustomsOfficeId() {
		return departCustomsOfficeId;
	}

	public void setDepartCustomsOfficeId(Long departCustomsOfficeId) {
		this.departCustomsOfficeId = departCustomsOfficeId;
	}

	@Column(name = ColumnNames.DEST_CUSTOMS_OFFICE_REF_NUM, length = 8)
	public String getDestCustomsOfficeRefNum() {
		return destCustomsOfficeRefNum;
	}

	public void setDestCustomsOfficeRefNum(String destCustomsOfficeRefNum) {
		this.destCustomsOfficeRefNum = destCustomsOfficeRefNum;
	}

	@ManyToOne
	@JoinColumn(name = ColumnNames.DEST_CUSTOMS_OFFICE_ID)
	public CustomsOffice getDestCustomsOffice() {
		return destCustomsOffice;
	}

	public void setDestCustomsOffice(CustomsOffice destCustomsOffice) {
		this.destCustomsOffice = destCustomsOffice;
	}

	@Column(name = ColumnNames.DEST_CUSTOMS_OFFICE_ID, insertable = false, updatable = false)
	public Long getDestCustomsOfficeId() {
		return destCustomsOfficeId;
	}

	public void setDestCustomsOfficeId(Long destCustomsOfficeId) {
		this.destCustomsOfficeId = destCustomsOfficeId;
	}

	@Column(name = ColumnNames.CONTROL_RESULT_CODE, length = 2)
	public String getControlResultCode() {
		return controlResultCode;
	}

	public void setControlResultCode(String controlResultCode) {
		this.controlResultCode = controlResultCode;
	}

	@ManyToOne
	@JoinColumn(name = ColumnNames.CONTROL_RESULT_ID)
	public MasterData getControlResult() {
		return controlResult;
	}

	public void setControlResult(MasterData controlResult) {
		this.controlResult = controlResult;
	}

	@Column(name = ColumnNames.CONTROL_RESULT_ID, insertable = false, updatable = false)
	public Long getControlResultId() {
		return controlResultId;
	}

	public void setControlResultId(Long controlResultId) {
		this.controlResultId = controlResultId;
	}

	@Column(name = ColumnNames.CONTROL_DATE_LIMIT)
	public Date getControlDateLimit() {
		return controlDateLimit;
	}

	public void setControlDateLimit(Date controlDateLimit) {
		this.controlDateLimit = controlDateLimit;
	}

	@Column(name = ColumnNames.REPRESENTATIVE_NAME, length = 35)
	public String getRepresentativeName() {
		return representativeName;
	}

	public void setRepresentativeName(String representativeName) {
		this.representativeName = representativeName;
	}

	@Column(name = ColumnNames.REPRESENTATIVE_CAPACITY, length = 35)
	public String getRepresentativeCapacity() {
		return representativeCapacity;
	}

	public void setRepresentativeCapacity(String representativeCapacity) {
		this.representativeCapacity = representativeCapacity;
	}

	@Column(name = ColumnNames.REPRESENTATIVE_CAPACITY_LNG, length = 2)
	public String getRepresentativeCapacityLng() {
		return representativeCapacityLng;
	}

	public void setRepresentativeCapacityLng(String representativeCapacityLng) {
		this.representativeCapacityLng = representativeCapacityLng;
	}

	@Column(name = ColumnNames.SEALS_NUMBER)
	public Integer getSealsNumber() {
		return sealsNumber;
	}

	public void setSealsNumber(Integer sealsNumber) {
		this.sealsNumber = sealsNumber;
	}

	@ManyToOne
	@JoinColumn(name = ColumnNames.CARRIER_SITE_ID)
	public PartnerSite getCarrierSite() {
		return carrierSite;
	}

	public void setCarrierSite(PartnerSite carrierSite) {
		this.carrierSite = carrierSite;
	}

	@Column(name = ColumnNames.CARRIER_SITE_ID, insertable = false, updatable = false)
	public Long getCarrierSiteId() {
		return carrierSiteId;
	}

	public void setCarrierSiteId(Long carrierSiteId) {
		this.carrierSiteId = carrierSiteId;
	}

	@Embedded
	public LocalCarrierSite getLocalCarrierSite() {
		return localCarrierSite;
	}

	public void setLocalCarrierSite(LocalCarrierSite localCarrierSite) {
		this.localCarrierSite = localCarrierSite;
	}

	@ManyToOne
	@JoinColumn(name = ColumnNames.SEC_CONSIGNOR_SITE_ID)
	public PartnerSite getSecConsignorSite() {
		return secConsignorSite;
	}

	public void setSecConsignorSite(PartnerSite secConsignorSite) {
		this.secConsignorSite = secConsignorSite;
	}

	@Column(name = ColumnNames.SEC_CONSIGNOR_SITE_ID, insertable = false, updatable = false)
	public Long getSecConsignorSiteId() {
		return secConsignorSiteId;
	}

	public void setSecConsignorSiteId(Long secConsignorSiteId) {
		this.secConsignorSiteId = secConsignorSiteId;
	}

	@Embedded
	public LocalSecConsignorSite getLocalSecConsignorSite() {
		return localSecConsignorSite;
	}

	public void setLocalSecConsignorSite(LocalSecConsignorSite localSecConsignorSite) {
		this.localSecConsignorSite = localSecConsignorSite;
	}

	@ManyToOne
	@JoinColumn(name = ColumnNames.SEC_CONSIGNEE_SITE_ID)
	public PartnerSite getSecConsigneeSite() {
		return secConsigneeSite;
	}

	public void setSecConsigneeSite(PartnerSite secConsigneeSite) {
		this.secConsigneeSite = secConsigneeSite;
	}

	@Column(name = ColumnNames.SEC_CONSIGNEE_SITE_ID, insertable = false, updatable = false)
	public Long getSecConsigneeSiteId() {
		return secConsigneeSiteId;
	}

	public void setSecConsigneeSiteId(Long secConsigneeSiteId) {
		this.secConsigneeSiteId = secConsigneeSiteId;
	}

	@Embedded
	public LocalSecConsigneeSite getLocalSecConsigneeSite() {
		return localSecConsigneeSite;
	}

	public void setLocalSecConsigneeSite(LocalSecConsigneeSite localSecConsigneeSite) {
		this.localSecConsigneeSite = localSecConsigneeSite;
	}

	@Column(name = ColumnNames.ACCEPTANCE_DATE, length = 35)
	public Date getAcceptanceDate() {
		return acceptanceDate;
	}

	public void setAcceptanceDate(Date acceptanceDate) {
		this.acceptanceDate = acceptanceDate;
	}

	@Column(name = ColumnNames.NUMBER_OF_LOADING_LISTS)
	public Integer getNumberOfLoadingLists() {
		return numberOfLoadingLists;
	}

	public void setNumberOfLoadingLists(Integer numberOfLoadingLists) {
		this.numberOfLoadingLists = numberOfLoadingLists;
	}

	@Column(name = ColumnNames.CONTROL_DATE)
	public Date getControlDate() {
		return controlDate;
	}

	public void setControlDate(Date controlDate) {
		this.controlDate = controlDate;
	}

	@Column(name = ColumnNames.CONTROLLED_BY, length = 35)
	public String getControlledBy() {
		return controlledBy;
	}

	public void setControlledBy(String controlledBy) {
		this.controlledBy = controlledBy;
	}

	@Column(name = ColumnNames.CONTROLLED_BY_LNG, length = 2)
	public String getControlledByLng() {
		return controlledByLng;
	}

	public void setControlledByLng(String controlledByLng) {
		this.controlledByLng = controlledByLng;
	}

	@Embedded
	public LocalReturnCopyCo getLocalReturnCopyCo() {
		return localReturnCopyCo;
	}

	public void setLocalReturnCopyCo(LocalReturnCopyCo localReturnCopyCo) {
		this.localReturnCopyCo = localReturnCopyCo;
	}

	@ManyToOne
	@JoinColumn(name = ColumnNames.RETURN_COPY_CO_ID)
	public CustomsOffice getReturnCopyCoCustomsOffice() {
		return returnCopyCoCustomsOffice;
	}

	public void setReturnCopyCoCustomsOffice(CustomsOffice returnCopyCoCustomsOffice) {
		this.returnCopyCoCustomsOffice = returnCopyCoCustomsOffice;
	}

	@Column(name = ColumnNames.RETURN_COPY_CO_ID, insertable = false, updatable = false)
	public Long getReturnCopyCoCustomsOfficeId() {
		return returnCopyCoCustomsOfficeId;
	}

	public void setReturnCopyCoCustomsOfficeId(Long returnCopyCoCustomsOfficeId) {
		this.returnCopyCoCustomsOfficeId = returnCopyCoCustomsOfficeId;
	}

	@Column(name = ColumnNames.DATE_OF_CONTROL)
	public Date getDateOfControl() {
		return dateOfControl;
	}

	public void setDateOfControl(Date dateOfControl) {
		this.dateOfControl = dateOfControl;
	}

	@Column(name = ColumnNames.WRITE_OFF_DATE)
	public Date getWriteOffDate() {
		return writeOffDate;
	}

	public void setWriteOffDate(Date writeOffDate) {
		this.writeOffDate = writeOffDate;
	}

	@Embedded
	public LocalGuarantorSite getLocalGuarantorSite() {
		return localGuarantorSite;
	}

	public void setLocalGuarantorSite(LocalGuarantorSite localGuarantorSite) {
		this.localGuarantorSite = localGuarantorSite;
	}

	@Column(name = ColumnNames.RELEASE_REQUEST_DATE)
	public Date getReleaseRequestDate() {
		return releaseRequestDate;
	}

	public void setReleaseRequestDate(Date releaseRequestDate) {
		this.releaseRequestDate = releaseRequestDate;
	}

	@ManyToOne
	@JoinColumn(name = ColumnNames.STATUS_ID)
	public NCTSDeclStatus getStatus() {
		return status;
	}

	public void setStatus(NCTSDeclStatus status) {
		this.status = status;
	}

	@Column(name = ColumnNames.STATUS_ID, insertable = false, updatable = false)
	public Long getStatusId() {
		return statusId;
	}

	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	@Column(name = ColumnNames.CREATED_DATE)
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@ManyToOne
	@JoinColumn(name = ColumnNames.CREATOR_USER)
	public User getCreatorUser() {
		return creatorUser;
	}

	public void setCreatorUser(User creatorUser) {
		this.creatorUser = creatorUser;
	}

	@Column(name = ColumnNames.MODIFIED_DATE)
	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	@ManyToOne
	@JoinColumn(name = ColumnNames.MODIFIER_USER)
	public User getModifierUser() {
		return modifierUser;
	}

	public void setModifierUser(User modifierUser) {
		this.modifierUser = modifierUser;
	}

	@OneToMany(mappedBy = NCTSDeclItem.AttributeNames.NCTS_DECLARATION, cascade = CascadeType.ALL)
	public List<NCTSDeclItem> getNctsDeclarationItems() {
		return nctsDeclarationItems;
	}

	public void setNctsDeclarationItems(List<NCTSDeclItem> nctsDeclarationItems) {
		this.nctsDeclarationItems = nctsDeclarationItems;
	}

	@OneToMany(mappedBy = NCTSDeclTransitCustomsOffice.AttributeNames.NCTS_DECLARATION, cascade = CascadeType.ALL)
	public List<NCTSDeclTransitCustomsOffice> getTransitionCustomsOffices() {
		return transitionCustomsOffices;
	}

	public void setTransitionCustomsOffices(List<NCTSDeclTransitCustomsOffice> transitionCustomsOffices) {
		this.transitionCustomsOffices = transitionCustomsOffices;
	}

	@OneToMany(mappedBy = NCTSDeclSeal.AttributeNames.NCTS_DECLARATION, cascade = CascadeType.ALL)
	public List<NCTSDeclSeal> getSeals() {
		return seals;
	}

	public void setSeals(List<NCTSDeclSeal> seals) {
		this.seals = seals;
	}

	@OneToMany(mappedBy = NCTSDeclGuarantee.AttributeNames.NCTS_DECLARATION, cascade = CascadeType.ALL)
	public List<NCTSDeclGuarantee> getGuarantees() {
		return guarantees;
	}

	public void setGuarantees(List<NCTSDeclGuarantee> guarantees) {
		this.guarantees = guarantees;
	}

	@OneToMany(mappedBy = NCTSDeclRejection.AttributeNames.NCTS_DECLARATION, cascade = CascadeType.ALL)
	public List<NCTSDeclRejection> getRejections() {
		return rejections;
	}

	public void setRejections(List<NCTSDeclRejection> rejections) {
		this.rejections = rejections;
	}

	@OneToMany(mappedBy = NCTSDeclGuaranteeInvalid.AttributeNames.NCTS_DECLARATION, cascade = CascadeType.ALL)
	public List<NCTSDeclGuaranteeInvalid> getInvalidGuarantees() {
		return invalidGuarantees;
	}

	public void setInvalidGuarantees(List<NCTSDeclGuaranteeInvalid> invalidGuarantees) {
		this.invalidGuarantees = invalidGuarantees;
	}

	@OneToMany(mappedBy = NCTSDeclRoute.AttributeNames.NCTS_DECLARATION, cascade = CascadeType.ALL)
	public List<NCTSDeclRoute> getRoutes() {
		return routes;
	}

	public void setRoutes(List<NCTSDeclRoute> routes) {
		this.routes = routes;
	}
}
