<?php
$model = "";

function get_string_between($string, $start, $end){
    $string = ' ' . $string;
    $ini = strpos($string, $start);
    if ($ini == 0) return '';
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
}

function toCamelCase($string)
{
	if ($string == null) return $string;
	$result = "";
	$i = 0;
	while($i < strlen($string))
	{
		$substring = substr($string, $i, 1);
		if ($substring == "_"){
			$result .= strtoupper(substr($string, ($i + 1), 1));
			$i++;
		} else
			$result .= strtolower($substring);

		$i++;
	}
	return $result;
}
$id = 0;

function readFileLines($fileName){
	$data = array('name' => array(), 'length' => array(), 'type' => array());
	$handle = fopen($fileName, "r");
	global $id;
	global $data;
	if ($handle) {
		while (($line = fgets($handle)) !== false) {
			$s = explode("$", $line);
			if ($s[2] != "String" && 
				$s[2] != "Long" && 
				$s[2] != "Date" && 
				$s[2] != "Integer" && 
				$s[2] != "Double" &&
				$s[2] != "boolean")
			{
				if (strpos(strtoupper($s[1]), "_CODE"))
					$s[0] = str_replace("_CODE", "", strtoupper($s[0]));

				else if (strpos(strtoupper($s[1]), "_ID"))
					$s[0] = str_replace("_ID", "", strtoupper($s[0]));
				
				$data[$id]['name'] = strtoupper($s[0]."_CODE");
				$data[$id]['length'] = $s[1];
				$data[$id]['type'] = "String";
				$id++;
				$data[$id]['name'] = strtoupper($s[0]."_ID");
				$data[$id]['length'] = $s[1];
				$data[$id]['type'] = "Long";
				$id++;
			}
			$data[$id]['name'] = $s[0];
			$data[$id]['length'] = $s[1];
			$data[$id]['type'] = $s[2];
			$id++;
		}
		fclose($handle);
	} else {
	}
}

function generateColumnNames(){
	global $id;
	global $data;
	global $model;
	$model .= "public static class ColumnNames {\n";
	for ($i = 0; $i < $id; $i++) {
		$model .= "public static final String ".strtoupper($data[$i]['name'])." = \"".strtolower($data[$i]['name'])."\";\n";
	}
	$model .= "private ColumnNames() {
		// intentionally empty
	}";
	$model .= "}\n";	
}

function generateAttributes(){
	global $id;
	global $data;
	global $model;
	$model .= "public static class AttributeNames {\n";
	for ($i = 0; $i < $id; $i++) {
		$model .= "public static final String ".strtoupper($data[$i]['name'])." = \"".toCamelCase($data[$i]['name'])."\";\n";
	}
	$model .= "private AttributeNames() {
		// intentionally empty
	}";
	$model .= "}\n";	
}

function generateLocalAttributes(){
	global $id;
	global $data;
	global $model;
	for ($i = 0; $i < $id; $i++) {
		$model .= "private ".$data[$i]['type']." ".toCamelCase($data[$i]['name']).";\n";
	}
	$model .= "\n";
}

function generateGetSet(){
	global $id;
	global $model;
	global $data;
	for ($i = 0; $i < $id; $i++) {
		if ($data[$i]['type'] == "boolean"){
			$model .= "
			@Column(name = ColumnNames.".strtoupper($data[$i]['name']).")
			public boolean is".ucfirst(toCamelCase($data[$i]['name']))."() {
				return ".toCamelCase($data[$i]['name']).";
			}
		
			public void set".ucfirst(toCamelCase($data[$i]['name']))."(boolean ".toCamelCase($data[$i]['name']).") {
				this.".toCamelCase($data[$i]['name'])." = ".toCamelCase($data[$i]['name']).";
			}";
			die();
		}
		if ($data[$i]['type'] == "String" || $data[$i]['type'] == "Long" || $data[$i]['type'] == "Date" || $data[$i]['type'] == "Integer" || $data[$i]['type'] == "Double"){
			$model .= "
			@Column(name = ColumnNames.".strtoupper($data[$i]['name']);
			if ($data[$i]['type'] == "Long") $model .= ", insertable = false, updatable = false";
			if ($data[$i]['type'] == "String") $model .= ", length = ".get_string_between($data[$i]['length'], "(", ")");
			
			$model .= ")
			public ".$data[$i]['type']." get".ucfirst(toCamelCase($data[$i]['name']))."() {
				return ".toCamelCase($data[$i]['name']).";
			}
		
			public void set".ucfirst(toCamelCase($data[$i]['name']))."(".$data[$i]['type']." ".toCamelCase($data[$i]['name']).") {
				this.".toCamelCase($data[$i]['name'])." = ".toCamelCase($data[$i]['name']).";
			}
			";     
		} else {
			$model .= "
			@ManyToOne
			@Column(name = ColumnNames.".strtoupper($data[$i]['name']).")
			public ".$data[$i]['type']." get".ucfirst(toCamelCase($data[$i]['name']))."() {
				return ".toCamelCase($data[$i]['name']).";
			}
		
			public void set".ucfirst(toCamelCase($data[$i]['name']))."(".$data[$i]['type']." ".toCamelCase($data[$i]['name']).") {
				this.".toCamelCase($data[$i]['name'])." = ".toCamelCase($data[$i]['name']).";
			}
			";  
		}
	}
}

$model .= "@Entity
@Table(name = NCTSDecl.TABLE_NAME)
public class NCTSDecl {

	public static final String TABLE_NAME = \"ncts_declaration\";\n";

readFileLines("asd.txt");
generateColumnNames();
generateAttributes();
generateLocalAttributes();
generateGetSet();
$model .= "}";
file_put_contents("ggg.java", $model);
?>